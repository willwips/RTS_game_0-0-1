# PAF : battle for Corma
![Vignette du jeu](sources/pictures/map/vignette.png)

## Présentation du projet

Ce jeu a été fait dans le cadre d'un coucours de NSI. Nous avons souhaité faire un projet ambitieux et pour ce faire nous avons voulu faire un jeu de type RTS en s'inspirant de jeux tels que Starcraft ou la série des Age of Empire.

Dans ce jeu s'affrontent deux joueurs, l'un bleu et l'autre rouge, le but étant de détruire tous les batiments adverse pour gagner. Pour ce faire il faut miner des ressources avec des workers, produire une armée et attaquer son adversaire.

## Installation

Après avoir télechargé le dossier, installer les modules dans le requierment.txt ainsi qu'un interprèteur c++ ( https://visualstudio.microsoft.com/fr/visual-cpp-build-tools/ ).

## Lancement du jeu

Pour lancer le jeu, vous devez lancer launcher.py, vous pouvez créer un serveur ou en rejoindre un dans votre réseau privé, puis lancer une partie en cliquant sur le bouton start lorsque vous le souhaiterez

## En jeu

![Image indiquant les commandes en jeu](sources/pictures/sprite/RTS_img_aide.png)
