import multiprocessing
import threading
import time

import pygame
import math
import sys

import launcher
from graphic import *
import troops
import buildings
import UI
import numpy as np
import network


class GameMaster:
    def __init__(self, clock, ip):
        self.clock = clock
        self.ntw = network.network(ip, 65432,self)  # 192.168.182.8
        self.inrun = False
        self.sizemap = 9000

    def start(self):
        self.ntw.Emission.send('START')
        self.ntw.Emission.send('GETTEAM')

    def run(self):
        self.inrun = True
        # Déclaration de la vitesse de la caméra
        self.horizontal_speed = 40
        self.vertical_speed = 40

        # Initialisation des offset (pour la caméra)
        self.offset_x = 0
        self.offset_y = 0

        # Varible permettant d'acceder aux informations de la classe Graphics
        self.graphic = Graphics()
        self.map = buildings.map_()
        self.menu_btn = UI.Button("building_option", (50, 50), 0, 0, "get_menu", self, 0, 0, -3)
        self.fence = Fence()

        # Initialisation des dictionnaires comportant les sprites du jeu
        self.sprite_list: dict = {-2: self.map}
        self.clickable_list: dict[int] = {}
        self.troop = {}
        self.building = {}

        # Initialisation de la grille de brouillard
        self.brouillard_grille = [[0] * int(self.sizemap/10) for i in range(int(self.sizemap/10))]
        self.a = 0
        self.minerai = []
        
        # Fonction permettant la création de minerais d'or
        def create_minerai_gold(coo_x,coo_y):
            self.sprite_list[100000000000+self.a] = buildings.Gold(100,coo_x*200+75,coo_y*200+75,self,100000000000+self.a)
            self.minerai.append(self.sprite_list[100000000000+self.a])

            self.a+=1
        
        # Fonction permettant la création de minerais de fer
        def create_minerai_fer(coo_x,coo_y):
            self.sprite_list[100000000000+self.a] = buildings.Iron(1000,coo_x*200+1,coo_y*200+1,self,100000000000+self.a)
            self.minerai.append(self.sprite_list[100000000000+self.a])
            self.a+=1
            self.sprite_list[100000000000+self.a] = buildings.Iron(1000,coo_x*200+149,coo_y*200+149,self,100000000000+self.a)
            self.minerai.append(self.sprite_list[100000000000+self.a])

            self.a+=1
            self.sprite_list[100000000000+self.a] = buildings.Iron(1000,coo_x*200+149,coo_y*200+1,self,100000000000+self.a)
            self.minerai.append(self.sprite_list[100000000000+self.a])

            self.a+=1
            self.sprite_list[100000000000+self.a] = buildings.Iron(1000,coo_x*200+1,coo_y*200+149,self,100000000000+self.a)
            self.minerai.append(self.sprite_list[100000000000+self.a])

            self.a+=1

        # Création des minerais sur la map
        create_minerai_fer(0,44)
        create_minerai_fer(1,44)
        create_minerai_fer(0,43)

        create_minerai_fer(7,37)
        create_minerai_fer(6,37)
        create_minerai_fer(7,38)
        create_minerai_gold(7,39)
        create_minerai_gold(5,37)

        create_minerai_fer(0,36)
        create_minerai_fer(0,37)
        create_minerai_fer(0,35)

        create_minerai_fer(2,31)
        create_minerai_fer(1,31)
        create_minerai_fer(3,31)
        create_minerai_gold(0,31)

        create_minerai_gold(5,26)
        create_minerai_gold(5,25)
        create_minerai_fer(4,25)
        create_minerai_fer(4,26)
        create_minerai_fer(3,26)
        create_minerai_fer(3,25)

        create_minerai_gold(5,19)
        create_minerai_gold(5,18)
        create_minerai_fer(4,19)
        create_minerai_fer(4,18)
        create_minerai_fer(3,19)
        create_minerai_fer(3,18)

        create_minerai_gold(3,4)
        create_minerai_gold(4,3)
        create_minerai_gold(5,4)
        create_minerai_gold(4,5)
        create_minerai_fer(2,5)
        create_minerai_fer(2,4)
        create_minerai_fer(2,3)
        create_minerai_fer(3,2)
        create_minerai_fer(4,2)
        create_minerai_fer(5,2)
        create_minerai_fer(6,5)
        create_minerai_fer(6,4)
        create_minerai_fer(6,3)
        create_minerai_fer(3,6)
        create_minerai_fer(4,6)
        create_minerai_fer(5,6)

        create_minerai_fer(8,44)
        create_minerai_fer(7,44)
        create_minerai_fer(9,44)

        create_minerai_fer(16,44)
        create_minerai_fer(17,44)
        create_minerai_fer(18,44)
        create_minerai_fer(19,44)
        create_minerai_fer(20,44)
        create_minerai_gold(21,44)
        create_minerai_gold(21,43)

        create_minerai_gold(20,36)
        create_minerai_gold(24,36)
        create_minerai_fer(21,36)
        create_minerai_fer(22,36)
        create_minerai_fer(23,36)

        create_minerai_fer(28,44)
        create_minerai_fer(27,44)
        create_minerai_fer(26,44)
        create_minerai_fer(25,44)
        create_minerai_fer(24,44)
        create_minerai_gold(23,44)
        create_minerai_gold(23,43)

        create_minerai_gold(39,40)
        create_minerai_gold(40,41)
        create_minerai_gold(40,39)
        create_minerai_gold(41,40)
        create_minerai_fer(39,38)
        create_minerai_fer(40,38)
        create_minerai_fer(41,38)
        create_minerai_fer(38,39)
        create_minerai_fer(38,40)
        create_minerai_fer(38,41)

        create_minerai_fer(39,42)
        create_minerai_fer(40,42)
        create_minerai_fer(41,42)
        create_minerai_fer(42,39)
        create_minerai_fer(42,40)
        create_minerai_fer(42,41)

        create_minerai_gold(39, 26)
        create_minerai_gold(39, 25)
        create_minerai_fer(40, 25)
        create_minerai_fer(40, 26)
        create_minerai_fer(41, 26)
        create_minerai_fer(41, 25)

        create_minerai_gold(39, 19)
        create_minerai_gold(39, 18)
        create_minerai_fer(40, 19)
        create_minerai_fer(40, 18)
        create_minerai_fer(41, 19)
        create_minerai_fer(41, 18)

        create_minerai_fer(42,13)
        create_minerai_fer(43,13)
        create_minerai_fer(41,13)
        create_minerai_gold(44,13)

        create_minerai_fer(44,9)
        create_minerai_fer(44,8)
        create_minerai_fer(44,7)

        create_minerai_fer(37,7)
        create_minerai_fer(38,7)
        create_minerai_fer(37,6)
        create_minerai_gold(39,7)
        create_minerai_gold(37,5)

        create_minerai_fer(44,0)
        create_minerai_fer(44,1)
        create_minerai_fer(43,0)

        create_minerai_fer(36, 0)
        create_minerai_fer(37, 0)
        create_minerai_fer(35, 0)

        create_minerai_fer(28, 0)
        create_minerai_fer(27, 0)
        create_minerai_fer(26, 0)
        create_minerai_fer(25, 0)
        create_minerai_fer(24, 0)
        create_minerai_gold(23, 0)
        create_minerai_gold(23, 1)

        create_minerai_gold(24, 8)
        create_minerai_gold(20, 8)
        create_minerai_fer(23, 8)
        create_minerai_fer(22, 8)
        create_minerai_fer(21, 8)

        create_minerai_fer(16, 0)
        create_minerai_fer(17, 0)
        create_minerai_fer(18, 0)
        create_minerai_fer(19, 0)
        create_minerai_fer(20, 0)
        create_minerai_gold(21, 0)
        create_minerai_gold(21, 1)

        create_minerai_fer(21, 24)
        create_minerai_fer(21, 20)
        create_minerai_gold(20, 20)

        create_minerai_fer(20, 21)
        create_minerai_fer(20, 23)
        create_minerai_gold(20, 24)

        create_minerai_fer(23, 20)
        create_minerai_fer(24, 20)
        create_minerai_gold(24, 20)

        create_minerai_fer(24, 23)
        create_minerai_fer(23, 24)
        create_minerai_gold(24, 24)

        create_minerai_fer(22, 30)
        create_minerai_fer(23, 30)
        create_minerai_fer(21, 30)
        create_minerai_gold(22, 31)

        create_minerai_fer(14, 21)
        create_minerai_fer(14, 22)
        create_minerai_fer(14, 23)
        create_minerai_gold(13, 22)

        create_minerai_fer(22, 14)
        create_minerai_fer(23, 14)
        create_minerai_fer(21, 14)
        create_minerai_gold(22, 13)

        create_minerai_fer(30, 21)
        create_minerai_fer(30, 22)
        create_minerai_fer(30, 23)
        create_minerai_gold(31, 22)


        self.resolution_screen = self.graphic.screen.get_size()
        self.click_sprite: troops.worker = None
        self.sprite_clicked = False
        self.actived_obj = []

        self.order = []
        
        # Initialisation d'attributs
        self.supply_max = 7
        self.supply = 0
        self.iron = 0
        self.gold = 0
        
        self.worker = 0
        self.max_worker = 10
        
        # Nombre de building du joueur; ceci est la condition de fin de partie (0 --> fin de partie)
        self.nb_buildings=0

        # Liste qui les gère les cooldowns des fonctions
        self.cooldown = []

        # Attrbuts permettant la fin de partie
        self.end_of_game_bol = False
        self.loser = None

        # Initialisation de la disponibilité d'unités et de leur prix pour les débloquer
        self.low_cost_unlocked = False
        self.low_cost_unlock_cost = (200, 0)

        self.kamikaze_unlocked = False
        self.kamikaze_unlock_cost = (200, 0)

        self.fast_troop_unlocked = False
        self.fast_troop_unlock_cost = (200, 0)

        self.archery_unlocked = False
        self.archery_unlock_cost = (200, 0)

        self.fast_troop_archery_unlocked = False
        self.fast_troop_archery_unlock_cost = (400, 15)

        self.siege_troop_unlocked = False
        self.siege_troop_unlock_cost = (500, 20)

        self.tank_unlocked = False
        self.tank_unlock_cost = (400, 20)

        self.debuff_unlocked = False
        self.debuff_unlock_cost = (300, 10)

        self.cannon_unlocked = False
        self.cannon_unlock_cost = (300, 5)

        # Initialisaiton des prix de tous les objets du jeu
        self.worker_cost = (50, 0)
        self.workerBis_cost = (50, 0)
        self.workerTrd_cost = (50, 1)
        self.kamikaze_cost = (70, 5)
        self.fast_troop_cost = (70, 0)
        self.archery_cost = (80, 1)
        self.cannon_cost = (400, 15)
        self.low_cost_cost = (50, 0)
        self.wall_cost = (100, 0)
        self.debuff_cost = (110, 10)
        self.fast_troop_archery_cost = (115, 5)
        self.tank_cost = (225, 5)
        self.siege_troop_cost = (300, 10)
        self.healer_cost = (200, 15)
        self.nexus_cost = (500, 0)
        self.supply_cost = (175, 0)
        self.upgrader_cost = (300, 0)
        self.upgraderBis_cost = (300, 25)
        self.upgraderTrd_cost = (
            300, 50)
        self.producer_cost = (200, 0)
        self.producerBis_cost = (200, 25)

        # w, h --> Récupère la longueur et largeur de l'écran
        w, h = pygame.display.get_surface().get_size()
        
        # Attributs permettant d'afficher du texte sur l'écran
        self.ressources_ui = UI.Button("RTS_ressources", (w, h*0.90), 0, 0, 0, self, 0, 0, -9)
        self.iron_text = pygame.font.SysFont("arial", int(w*0.014)).render(str(int(self.iron)), False, (255, 255, 255))
        self.gold_text = pygame.font.SysFont("arial", int(w*0.014)).render(str(int(self.gold)), False, (255, 255, 255))        
        self.supply_text = pygame.font.SysFont("arial", int(w*0.009)).render(str(int(self.supply))+" / "+str(int(self.supply_max)), False, (255, 255, 255))              
        self.sprite_list[-9] = self.ressources_ui
    
        # Image pour l'écran de fin de partie)
        self.img_victory_blue = pygame.image.load(f'pictures/sprite/RTS_victory_blue.png')
        self.img_victory_blue = pygame.transform.scale(self.img_victory_blue, (w, h))
        self.img_victory_red = pygame.image.load(f'pictures/sprite/RTS_victory_red.png')
        self.img_victory_red = pygame.transform.scale(self.img_victory_red, (w, h))

        self.img_defeat_blue = pygame.image.load(f'pictures/sprite/RTS_defeat_blue.png')
        self.img_defeat_blue = pygame.transform.scale(self.img_defeat_blue, (w, h))
        self.img_defeat_red = pygame.image.load(f'pictures/sprite/RTS_defeat_red.png')
        self.img_defeat_red = pygame.transform.scale(self.img_defeat_red, (w, h))


        # Images de buildings en fonction de l'équipe
        self.list_img = [["RTS_Nexus_blue", "RTS_Upgrader_blue", "RTS_UpgraderBis_blue", "RTS_Producer_blue", "RTS_ProducerBis_blue", "RTS_healer_blue", "RTS_supply_blue", "RTS_cannon_blue", "RTS_wall_blue"], ["RTS_Nexus_red", "RTS_Upgrader_red", "RTS_UpgraderBis_red", "RTS_Producer_red", "RTS_ProducerBis_red", "RTS_healer_red", "RTS_supply_red", "RTS_cannon_red", "RTS_wall_blue"]]

        self.blit_img_nex_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][0]}.png').convert_alpha()
        self.blit_img_nex_blue = pygame.transform.scale(self.blit_img_nex_blue, (140, 140))
        self.blit_img_nex_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][0]}.png').convert_alpha()
        self.blit_img_nex_red = pygame.transform.scale(self.blit_img_nex_red, (140, 140))

        self.blit_img_upg_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][1]}.png').convert_alpha()
        self.blit_img_upg_blue = pygame.transform.scale(self.blit_img_upg_blue, (140, 140))
        self.blit_img_upg_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][1]}.png').convert_alpha()
        self.blit_img_upg_red = pygame.transform.scale(self.blit_img_upg_red, (140, 140))
    
        self.blit_img_upgBis_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][2]}.png').convert_alpha()
        self.blit_img_upgBis_blue = pygame.transform.scale(self.blit_img_upgBis_blue, (140, 140))
        self.blit_img_upgBis_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][2]}.png').convert_alpha()
        self.blit_img_upgBis_red = pygame.transform.scale(self.blit_img_upgBis_red, (140, 140))

        self.blit_img_prod_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][3]}.png').convert_alpha()
        self.blit_img_prod_blue = pygame.transform.scale(self.blit_img_prod_blue, (140, 140))
        self.blit_img_prod_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][3]}.png').convert_alpha()
        self.blit_img_prod_red = pygame.transform.scale(self.blit_img_prod_red, (140, 140))

        self.blit_img_prodBis_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][4]}.png').convert_alpha()
        self.blit_img_prodBis_blue = pygame.transform.scale(self.blit_img_prodBis_blue, (140, 140))
        self.blit_img_prodBis_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][4]}.png').convert_alpha()
        self.blit_img_prodBis_red = pygame.transform.scale(self.blit_img_prodBis_red, (140, 140))

        self.blit_img_healer_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][5]}.png').convert_alpha()
        self.blit_img_healer_blue = pygame.transform.scale(self.blit_img_healer_blue, (80, 110))
        self.blit_img_healer_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][5]}.png').convert_alpha()
        self.blit_img_healer_red = pygame.transform.scale(self.blit_img_healer_red, (80, 110))

        self.blit_img_supply_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][6]}.png').convert_alpha()
        self.blit_img_supply_blue = pygame.transform.scale(self.blit_img_supply_blue, (140, 140))
        self.blit_img_supply_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][6]}.png').convert_alpha()
        self.blit_img_supply_red = pygame.transform.scale(self.blit_img_supply_red, (140, 140))

        self.blit_img_cannon_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][7]}.png').convert_alpha()
        self.blit_img_cannon_blue = pygame.transform.scale(self.blit_img_cannon_blue, (60, 60))
        self.blit_img_cannon_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][7]}.png').convert_alpha()
        self.blit_img_cannon_red = pygame.transform.scale(self.blit_img_cannon_red, (60, 60))

        self.blit_img_wall_blue = pygame.image.load(f'pictures/sprite/{self.list_img[0][8]}.png').convert_alpha()
        self.blit_img_wall_blue = pygame.transform.scale(self.blit_img_wall_blue, (130, 130))
        self.blit_img_wall_red = pygame.image.load(f'pictures/sprite/{self.list_img[1][8]}.png').convert_alpha()
        self.blit_img_wall_red = pygame.transform.scale(self.blit_img_wall_red, (130, 130))




        # Position du joueur bleu sur la map
        if self.ntw.team[0] == 'BLUE':
            self.offset_x = 9000 - self.resolution_screen[0]
            self.offset_y = 0

            self.create_nexus(200*43+25,225)

            self.create_workerBis(200*42,200)
            self.create_worker(200*43,400)

        # Position du joueur rouge sur la map 
        if self.ntw.team[0] == 'RED':
            self.offset_y = 9000- self.resolution_screen[1]
            self.offset_x = 0

            self.create_nexus(225,200*43+25)
            self.create_workerBis(200,200*42)
            self.create_worker(400,200*43)

    # Fonction qui gère les actions du joueurs sur son clavier/sourie
    def getevent(self):
        for event in pygame.event.get():
            # Permet de quitter le programme avec une intéraction forcée
            if event.type == pygame.QUIT:
                self.ntw.send('FIN')

                pygame.quit()
                launcher.launch()

            #  Permet d'effectuer des actions si la partie n'est pas fini
            if not self.end_of_game_bol:
                # Vérifie si un bouton de clavier est pressé
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE: # Si échap est pressé affiche le menu
                        self.menu_btn.on_click()

                    if event.key == pygame.K_LCTRL: # Si ctrl est pressé quitte le jeu
                        self.ntw.Emission.send('FIN')
                        pygame.quit()
                        sys.exit()

                    if event.key == pygame.K_a:
                        self.iron += 50
                        self.gold += 10
                        self.supply_max += 3

                    if event.key == pygame.K_i:
                        self.create_nexus(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 62)

                    if event.key == pygame.K_o:
                        self.create_worker(pygame.mouse.get_pos()[0] + self.offset_x - 25,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 25)
                        
                    if event.key == pygame.K_x:
                        self.create_workerBis(pygame.mouse.get_pos()[0] + self.offset_x - 25,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 25)

                    if event.key == pygame.K_b:
                        self.create_workerTrd(pygame.mouse.get_pos()[0] + self.offset_x - 25,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 25)

                    if event.key == pygame.K_k:
                        self.create_kamikaze(pygame.mouse.get_pos()[0] + self.offset_x - 25,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 25)
                    
                    if event.key == pygame.K_d:
                        self.create_debuff(pygame.mouse.get_pos()[0] + self.offset_x - 25,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 25)
                    
                    if event.key == pygame.K_w:
                        self.create_cannon(pygame.mouse.get_pos()[0] + self.offset_x - 30,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 30)

                    if event.key == pygame.K_1:
                        self.create_wall(pygame.mouse.get_pos()[0] + self.offset_x - 30,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 30)

                    if event.key == pygame.K_l:
                        self.create_lowcost(pygame.mouse.get_pos()[0] + self.offset_x - 16,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 16)

                    if event.key == pygame.K_f:
                        self.create_fast_troop(pygame.mouse.get_pos()[0] + self.offset_x - 16,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 16)

                    if event.key == pygame.K_z:
                        self.create_archery(pygame.mouse.get_pos()[0] + self.offset_x - 16,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 16)

                    if event.key == pygame.K_p:
                        self.create_upgrader(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67)
                    if event.key == pygame.K_t:
                        self.create_wall(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67)
                    if event.key == pygame.K_c:
                        self.create_builder(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67)

                    if event.key == pygame.K_v:
                        self.create_builderBis(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67)
                        
                    if event.key == pygame.K_m:
                        self.create_upgraderBis(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 60)

                    if event.key == pygame.K_h:
                        self.create_healer(pygame.mouse.get_pos()[0] + self.offset_x - 44,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 50)


                    if event.key == pygame.K_q: # Si q est pressé, l'unité attaque
                        for i in self.sprite_list.values():
                            if i.id > 0:

                                if i.rect.collidepoint(pygame.mouse.get_pos()):
                                    self.attaque(self.actived_obj[0].id, i.id)
                                    break

                    if event.key == pygame.K_e: # Si e est pressé sur un minerai et ensuite sur un nexus (ou vice-versa), le worker mine en donnant les ressources au nexus en question
                        for i in self.minerai:

                            if i.rect.collidepoint(pygame.mouse.get_pos()): # Vérification de la collision entre la sourie et le minerai
                                self.mine(self.actived_obj[0].id,i.id)

                        for i in self.clickable_list.values():
                            if i.__class__.__name__ == 'nexus':
                                if i.rect.collidepoint(pygame.mouse.get_pos()): # Vérification de la collision entre la sourie et le nexus
                                    self.target_nexus(self.actived_obj[0].id,i.id)

                # lorsqu'on clique quelque part
                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1: # --> Clique gauche
                        fusion = False
                        
                        if self.actived_obj: # Si un objet est présent dans la liste
                            if self.actived_obj[0].action: # Si l'objet de la liste fait une action
                                n=0

                                if self.actived_obj[0].action[0] != "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON" and self.actived_obj[0].action[0] != "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS" and self.actived_obj[0].action[0] != "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD":

                                    for i in self.clickable_list.values():
                                        if not i.click(pygame.mouse.get_pos(),False):
                                            n+=1

                                if n==len(self.clickable_list.values()) or self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON"  or self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS"  or self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD":
                                    if self.actived_obj[0].action[0] == "create_upgrader": # Création de l'upgrader
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67, 140, 140):
                                            self.create_upgrader_with_worker(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 67)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()
                                        
                                    elif self.actived_obj[0].action[0] == "create_nexus": # Création du nexus
                                        print('eee')

                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67, 140, 140):
                                            self.create_nexus_with_worker(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:


                                            self.No_space()
                                    
                                    elif self.actived_obj[0].action[0] == "create_healer": # Création du healer
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 44,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 50, 140, 140):
                                            self.create_healer_with_workerTrd(pygame.mouse.get_pos()[0] + self.offset_x - 44,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 50)
                                            del self.actived_obj[0].action[0]
                                        else:

                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_upgraderBis": # Création de l'upgraderBis
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 67, 140, 140):
                                            self.create_upgraderBis_with_worker(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_cannon": # Création du canon
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 30,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 30, 140, 140):
                                            self.create_cannon_with_workerTrd(pygame.mouse.get_pos()[0] + self.offset_x - 30,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 30)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_Producer": # Création du producteur
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 62, 140, 140):
                                            self.create_Producer_with_worker(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_ProducerBis": # Création du producteurBis
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 62, 140, 140):
                                            self.create_ProducerBis_with_workerBis(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_wall": # Création du mur
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 62, 140, 140):
                                            self.create_wall_with_workerTrd(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                    pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "create_supply": # Création du supply
                                        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                        pygame.mouse.get_pos()[1] + self.offset_y - 62, 140, 140):
                                            self.create_supply_with_worker(pygame.mouse.get_pos()[0] + self.offset_x - 64,
                                                                        pygame.mouse.get_pos()[1] + self.offset_y - 62)
                                            del self.actived_obj[0].action[0]
                                        else:
                                            self.No_space()

                                    elif self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON": # Fusion entre les workers
                                        fusion = True
                                        for i in self.clickable_list.values():
                                            if i.__class__.__name__ == 'worker': # Vérifie la collision seulement avec l'objet dont la classe a pour nom worker
                                                if i.rect.collidepoint(pygame.mouse.get_pos()):
                                                    self.actived_obj[0].fusion(i.id)
                                                    break
                                        del self.actived_obj[0].action[0]
                                        
                                    elif self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS": # Fusion entre les workersBis
                                        fusion = True
                                        for i in self.clickable_list.values():
                                            if i.__class__.__name__ == 'workerBis': # Vérifie la collision seulement avec l'objet dont la classe a pour nom workerBis
                                                if i.rect.collidepoint(pygame.mouse.get_pos()):
                                                    self.actived_obj[0].fusion(i.id)
                                                    break
                                        del self.actived_obj[0].action[0]

                                    elif self.actived_obj[0].action[0] == "FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD": # Fusion entre les workersTrd
                                        fusion = True
                                        for i in self.clickable_list.values():
                                            if i.__class__.__name__ == 'workerTrd':# Vérifie la collision seulement avec l'objet dont la classe a pour nom workerTrd
                                                if i.rect.collidepoint(pygame.mouse.get_pos()):
                                                    self.actived_obj[0].fusion(i.id)
                                                    break
                                        del self.actived_obj[0].action[0]
                                else:
                                    try:

                                        del self.actived_obj[0].action[0]
                                    except:
                                        pass
                        self.sprite_clicked = False

                        # Vérifie si une troupe est cliqué, si c'est le cas, la stock dans une variable et l'active
                        if not fusion:
                            for i in self.clickable_list.values():

                                if i.click(event.pos):
                                    self.click_sprite = i
                                    self.sprite_clicked = True
                                    break
                            # sinon bouge le sprite
                            if not self.sprite_clicked:
                                pass
                            # erreur donc bon démerde toi
                    
                    elif event.button == 3: # Clique droit --> l'unité se déplace
                        self.move(self.actived_obj[0].id,
                                pygame.mouse.get_pos()[0] + self.offset_x - 19,
                                pygame.mouse.get_pos()[1] + self.offset_y - 18)

    def cameamotion(self):
        # Lorsque la souris est en dehors de l'écran, on bouge la caméra
        if pygame.mouse.get_pos()[0] <= 0:
            self.offset_x = max(1, self.offset_x - self.horizontal_speed)
        if pygame.mouse.get_pos()[0] >= pygame.display.get_surface().get_size()[0] - 10:
            self.offset_x = min(self.sizemap - self.resolution_screen[0], self.offset_x + self.horizontal_speed)
        if pygame.mouse.get_pos()[1] <= 0:
            self.offset_y = max(1, self.offset_y - self.vertical_speed)
        if pygame.mouse.get_pos()[1] >= pygame.display.get_surface().get_size()[1] - 10:
            self.offset_y = min(self.sizemap - self.resolution_screen[1], self.offset_y + self.vertical_speed)

    def ordernetwork(self):
        # voit si un ordre a été reçu, si oui l'ajoute a la liste
        try:
            self.order.append(self.ntw.msg[0].split('|'))
            del self.ntw.msg[0]
        except IndexError:
            pass

        if len(self.order) > 0:

            # si on arrive au moment ou l'ordre doit etre éxecuté, l'éxecute
            if int(self.order[0][0]) < time.time_ns():
                if int(self.order[0][1]) == 1:
                    # Ordre qui permet la création de troops ou buildings ou permet la fin de partie
                    if int(self.order[0][2]) == 1:
                        self.create_nexus_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 2:
                        self.create_worker_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]),1)
                    elif int(self.order[0][2]) == 3:
                        self.create_upgrader_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 4:
                        self.create_nexus_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 5:
                        self.create_worker_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]),2)
                    elif int(self.order[0][2]) == 6:
                        self.create_upgrader_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]),2)
                    elif int(self.order[0][2]) == 7:
                        self.create_upgraderBis_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 8:
                        self.create_upgraderBis_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 9:
                        self.create_kamikaze_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 10:
                        self.create_kamikaze_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 11:
                        self.create_debuff_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 12:
                        self.create_debuff_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 13:
                        self.create_lowcost_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 14:
                        self.create_lowcost_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 15:
                        self.create_healer_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 16:
                        self.create_healer_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 17:
                        self.create_builder_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 18:
                        self.create_builder_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 19:
                        self.create_supply_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 20:
                        self.create_supply_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 21:
                        self.create_builderBis_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 22:
                        self.create_builderBis_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 23:
                        self.create_fast_troop_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 24:
                        self.create_fast_troop_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 25:
                        self.create_archery_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 26:
                        self.create_archery_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 27:
                        self.create_workerBis_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 28:
                        self.create_workerBis_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 29:
                        self.create_tank_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 30:
                        self.create_tank_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 31:
                        self.create_cannon_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 32:
                        self.create_cannon_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 33:
                        self.create_workerTrd_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 34:
                        self.create_workerTrd_(int(self.order[0][3]), self.order[0][4], int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 35:
                        self.create_siege_troop_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 36:
                        self.create_siege_troop_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 37:
                        self.create_fast_troop_archery_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 39:
                        self.create_fast_troop_archery_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 40:                                                                                                                    
                        self.create_wall_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 1)
                    elif int(self.order[0][2]) == 41:
                        self.create_wall_(int(self.order[0][3]), int(self.order[0][4]), int(self.order[0][5]), 2)
                    elif int(self.order[0][2]) == 42:                                                                                                                    
                        self.end_of_game_(self.order[0][3])
                    elif int(self.order[0][2]) == 43:
                        self.end_of_game_(self.order[0][3])

                # Autres actions
                elif int(self.order[0][1]) == 2:
                    self.move_(self.order[0][4], self.order[0][2], self.order[0][3])
                elif int(self.order[0][1]) == 3:
                    self.suppr_sprite_(int(self.order[0][2]), self.order[0][3])
                elif int(self.order[0][1]) == 4:
                    self.attaque_(self.order[0][2], self.order[0][3])
                elif int(self.order[0][1]) == 5:
                    self.mine_(self.order[0][2],self.order[0][3])
                elif int(self.order[0][1]) == 6:
                    self.target_nexus_(self.order[0][2],self.order[0][3])
                del self.order[0]

    def update(self): # Met à jour le jeu
        t= time.time_ns()
        self.getevent()
        t1 = time.time_ns()
        self.cameamotion()
        t2 = time.time_ns()

        # Retire les sprites n'apparaisssant pas sur l'écran pour ne charger que ceux présent
        self.graphic.clear_on_screen_group()
        t3 = time.time_ns()

        for i in self.sprite_list.values():
            if i.update_pos(self.offset_x, self.offset_y):
                self.graphic.add_on_screen_group(i)
        t4 = time.time_ns()

        self.ordernetwork()
        n = 0
        t5 = time.time_ns()

        for i in self.cooldown:
            i[0] -= 1 / self.clock.get_fps()
            if i[0] <= 0:
                i[1]()
                del self.cooldown[n]
            n += 1
        t6 = time.time_ns()

        for i in list(self.sprite_list.values()):
            i._move()
            i.attack_()
            try:
                i.mine_()
            except:
                pass
        t7 = time.time_ns()

        self.graphic.draw()
        t8 = time.time_ns()

    def blit_pre_img(self, g): # Permet de montrer l'image du building que va créer le worker
        if self.ntw.team[0] == "BLUE":  
            if self.actived_obj[0].action[0] == "create_nexus":
                g.screen.blit(self.blit_img_nex_blue, [pygame.mouse.get_pos()[0] -  64,
                            pygame.mouse.get_pos()[1] -  62])
            
            elif self.actived_obj[0].action[0] == "create_upgrader":
                g.screen.blit(self.blit_img_upg_blue, [pygame.mouse.get_pos()[0] - 69,
                                            pygame.mouse.get_pos()[1] - 67])
       
            elif self.actived_obj[0].action[0] == "create_upgraderBis":
                g.screen.blit(self.blit_img_upgBis_blue, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])
            
            elif self.actived_obj[0].action[0] == "create_Producer":
                g.screen.blit(self.blit_img_prod_blue, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])
            
            elif self.actived_obj[0].action[0] == "create_ProducerBis":
                g.screen.blit(self.blit_img_prodBis_blue, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])

            elif self.actived_obj[0].action[0] == "create_healer":
                g.screen.blit(self.blit_img_healer_blue, [pygame.mouse.get_pos()[0] - 44,
                            pygame.mouse.get_pos()[1] - 50])
            
            elif self.actived_obj[0].action[0] == "create_supply":
                g.screen.blit(self.blit_img_supply_blue, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])

            elif self.actived_obj[0].action[0] == "create_cannon":
                g.screen.blit(self.blit_img_cannon_blue, [pygame.mouse.get_pos()[0] - 30,
                            pygame.mouse.get_pos()[1] - 30])

            elif self.actived_obj[0].action[0] == "create_wall":
                g.screen.blit(self.blit_img_wall_blue, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62]) 
        else:
            if self.actived_obj[0].action[0] == "create_nexus":
                g.screen.blit(self.blit_img_nex_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])
            
            elif self.actived_obj[0].action[0] == "create_upgrader":
                g.screen.blit(self.blit_img_upg_red, [pygame.mouse.get_pos()[0] - 69,
                                            pygame.mouse.get_pos()[1] - 67])
                
            elif self.actived_obj[0].action[0] == "create_upgraderBis":
                g.screen.blit(self.blit_img_upgBis_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1]- 62])
            
            elif self.actived_obj[0].action[0] == "create_Producer":
                g.screen.blit(self.blit_img_prod_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])
            
            elif self.actived_obj[0].action[0] == "create_ProducerBis":
                g.screen.blit(self.blit_img_prodBis_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])

            elif self.actived_obj[0].action[0] == "create_healer":
                g.screen.blit(self.blit_img_healer_red, [pygame.mouse.get_pos()[0] - 44,
                            pygame.mouse.get_pos()[1] - 50])
            
            elif self.actived_obj[0].action[0] == "create_supply":
                g.screen.blit(self.blit_img_supply_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62])
                
            elif self.actived_obj[0].action[0] == "create_cannon":
                g.screen.blit(self.blit_img_cannon_red, [pygame.mouse.get_pos()[0] - 30,
                            pygame.mouse.get_pos()[1] - 30])
                
            elif self.actived_obj[0].action[0] == "create_wall":
                g.screen.blit(self.blit_img_wall_red, [pygame.mouse.get_pos()[0] - 64,
                            pygame.mouse.get_pos()[1] - 62]) 
                        
    def attaque(self, id1, id2): # Envoie l'action au serveur
        self.ntw.send(f'4|{id1}|{id2}| ')

    def mine(self,id1,id2): # Envoie l'action au serveur
        self.ntw.send(f'5|{id1}|{id2}| ')

    def mine_(self,id1,id2): # Gère le minage avec un thread
        threading.Thread(target = self.troop[int(id1)].mine, args = (int(id2),)).start()
    
    def attaque_(self, id1, id2): # Une unité en attaque une autre
        self.sprite_list[int(id1)].attack(int(id2))
    
    def target_nexus(self,id1,id2): # Envoie l'action au serveur
        self.ntw.send(f'6|{id1}|{id2}| ')

    def target_nexus_(self,id1,id2): # Gère le nexus choisit pour le minage
        self.troop[int(id1)].target_nexus(self.sprite_list[int(id2)].x_dep,self.sprite_list[int(id2)].y_dep)
    
    def create_supply(self, x, y):  # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|19|{x}|{y}')
        else:
            self.ntw.send(f'1|20|{x}|{y}')

# CE GENRE DE FONCTIONS NE SERONT COMMENTES QU'UNE FOIS CAR C'EST TOUJOURS PAREIL

    def create_supply_(self, x, y, id,idteam):
        d = buildings.supply(x, y, self, id,idteam) # Initialisation de l'objet
        if self.fence.check_confirmation(d): # Si la posiiton sur laquelle doit être crée l'objet est libre ->
            if d.allyteam:   # Si le bâtiment est un bâtiment allié, alors le conteur de building augmente (à 0 fin de partie). Cette ligne n'a lieu que pour les objets de classe building
                self.nb_buildings += 1
                
            self.sprite_list[id] = d # Affiche l'objet en le mettant dans une liste de sprite
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'): # Vérifie l'équipe du joueur
                self.clickable_list[id] = d # Signifie que le sprite est cliquable pour effectuer des actions en l'ajoutant dans la liste
                
            self.building[d.id] = d # Associe l'id du building au building

    def create_worker(self, x, y,type = 1): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|2|{type}|{x},{y}')
        else:
            self.ntw.send(f'1|5|{type}|{x},{y}')

    def create_worker_(self, type, coo, id,idteam):
        coo = coo.split(',')
        w = troops.worker(float(coo[0]), float(coo[1]), self, id,idteam,type)
        if self.fence.check_confirmation(w):
            self.sprite_list[id] = w
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = w
            self.troop[w.id] = w

    def create_tank(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|29|{x}|{y}')
        else:
            self.ntw.send(f'1|30|{x}|{y}')

    def create_tank_(self, x, y, id,idteam):
        t = troops.tank(x, y, self, id,idteam)
        if self.fence.check_confirmation(t):
            self.sprite_list[id] = t
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = t
            self.troop[t.id] = t

    def create_workerBis(self, x, y,type = 1) : # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|27|{type}|{x},{y}')
        else:
            self.ntw.send(f'1|28|{type}|{x},{y}')

    def create_workerBis_(self, type, coo, id,idteam):

        coo = coo.split(',')
        wBis = troops.workerBis(float(coo[0]), float(coo[1]), self, id,idteam,type)
        if self.fence.check_confirmation(wBis):
            self.sprite_list[id] = wBis
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = wBis
            self.troop[wBis.id] = wBis

    def create_workerTrd(self, x, y,type = 1): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|33|{type}|{x},{y}')
        else:
            self.ntw.send(f'1|34|{type}|{x},{y}')

    def create_workerTrd_(self, type, coo, id,idteam):
        coo = coo.split(',')

        wTrd = troops.workerTrd(float(coo[0]), float(coo[1]), self, id,idteam,type)
        if self.fence.check_confirmation(wTrd):
            self.sprite_list[id] = wTrd
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = wTrd
            self.troop[wTrd.id] = wTrd

    def create_kamikaze(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|9|{x}|{y}')
        else:
            self.ntw.send(f'1|10|{x}|{y}')

    def create_kamikaze_(self, x, y, id,idteam):
        k = troops.kamikaze(x, y, self, id,idteam)
        if self.fence.check_confirmation(k):
            self.sprite_list[id] = k
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = k
            self.troop[k.id] = k

    def create_debuff(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|11|{x}|{y}')
        else:
            self.ntw.send(f'1|12|{x}|{y}')

    def create_debuff_(self, x, y, id,idteam):
        d = troops.debuff(x, y, self, id,idteam)
        if self.fence.check_confirmation(d):
            self.sprite_list[id] = d
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = d
            self.troop[d.id] = d

    def create_lowcost(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|13|{x}|{y}')
        else:
            self.ntw.send(f'1|14|{x}|{y}')

    def create_lowcost_(self, x, y, id,idteam):
        lc = troops.lowcost(x, y, self, id,idteam)
        if self.fence.check_confirmation(lc):
            self.sprite_list[id] = lc
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = lc
            self.troop[lc.id] = lc

    def create_fast_troop(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|23|{x}|{y}')
        else:
            self.ntw.send(f'1|24|{x}|{y}')

    def create_fast_troop_(self, x, y, id,idteam): 
        ft = troops.fast_troop(x, y, self, id,idteam)
        if self.fence.check_confirmation(ft):
            self.sprite_list[id] = ft
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = ft
            self.troop[ft.id] = ft
    
    def create_archery(self, x, y):
        if self.ntw.team[0] == 'BLUE': # Envoie une action au serveur en fonction de l'équipe
            self.ntw.send(f'1|25|{x}|{y}')
        else:
            self.ntw.send(f'1|26|{x}|{y}')

    def create_archery_(self, x, y, id,idteam):
        ar = troops.archery(x, y, self, id,idteam)
        if self.fence.check_confirmation(ar):
            self.sprite_list[id] = ar
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = ar
            self.troop[ar.id] = ar

    def create_cannon(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|31|{x}|{y}')
        else:
            self.ntw.send(f'1|32|{x}|{y}')

    def create_cannon_(self, x, y, id,idteam): 
        c = buildings.cannon(x, y, self, id,idteam)
        if self.fence.check_confirmation(c):
            self.sprite_list[id] = c
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = c
            self.troop[c.id] = c
    
    def create_wall(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|40|{x}|{y}')
        else:
            self.ntw.send(f'1|41|{x}|{y}')

    def create_wall_(self, x, y, id,idteam): 
        c = buildings.mur(x, y, self, id,idteam)
        if self.fence.check_confirmation(c):
            self.sprite_list[id] = c
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = c

    def create_nexus(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|1|{x}|{y}')
        else:
            self.ntw.send(f'1|4|{x}|{y}')

    def create_nexus_(self, x, y, id,idteam):
        # Création du nexus
        nex = buildings.nexus(x, y, self, id, idteam)
        nex.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(nex):
            if nex.allyteam:   
                self.nb_buildings += 1
            self.sprite_list[id] = nex
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = nex
            self.building[nex.id] = nex
    
    def create_builder(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|17|{x}|{y}')
        else:
            self.ntw.send(f'1|18|{x}|{y}')

    def create_builder_(self, x, y, id,idteam):
        # Création du builder
        bui = buildings.Producer(x, y, self, id, idteam)
        bui.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(bui):
            if bui.allyteam:   
                self.nb_buildings += 1
            self.sprite_list[id] = bui
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = bui
            self.building[bui.id] = bui
    
    def create_builderBis(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|21|{x}|{y}')
        else:
            self.ntw.send(f'1|22|{x}|{y}')

    def create_builderBis_(self, x, y, id,idteam):
        # Création du builder
        buiBis = buildings.ProducerBis(x, y, self, id, idteam)
        buiBis.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(buiBis):
            if buiBis.allyteam:   
                self.nb_buildings += 1
            self.sprite_list[id] = buiBis
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = buiBis
            self.building[buiBis.id] = buiBis
      
    def create_siege_troop(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|35|{x}|{y}')
        else:
            self.ntw.send(f'1|36|{x}|{y}')

    def create_siege_troop_(self, x, y, id,idteam):
        # Création du builder
        st = troops.siege_troop(x, y, self, id, idteam)
        st.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(st):
            self.sprite_list[id] = st
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = st
            self.troop[st.id] = st

    def create_fast_troop_archery(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|37|{x}|{y}')
        else:
            self.ntw.send(f'1|38|{x}|{y}')

    def create_fast_troop_archery_(self, x, y, id,idteam):
        # Création du builder
        fta = troops.fast_troop_archery(x, y, self, id, idteam)
        fta.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(fta):
            self.sprite_list[id] = fta
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = fta
            self.troop[fta.id] = fta

    def create_upgrader(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|3|{x}|{y}')
        else:
            self.ntw.send(f'1|6|{x}|{y}')

    def create_upgrader_(self, x, y, id,idteam):
        # Création du nexus
        upg = buildings.upgrader(x, y, self, id,idteam)
        upg.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(upg):
            if upg.allyteam:   
                self.nb_buildings += 1
            self.sprite_list[id] = upg
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = upg
            self.building[upg.id] = upg

    def create_upgraderBis(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|7|{x}|{y}')
        else:
            self.ntw.send(f'1|8|{x}|{y}')

    def create_upgraderBis_(self, x, y, id,idteam):
        # Création du nexus
        upgBis = buildings.upgraderBis(x, y, self, id,idteam)
        upgBis.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(upgBis):
            if upgBis.allyteam:   
                self.nb_buildings += 1
            self.sprite_list[id] = upgBis
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = upgBis
            self.building[upgBis.id] = upgBis

    def create_healer(self, x, y): # Envoie une action au serveur en fonction de l'équipe
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|15|{x}|{y}')
        else:
            self.ntw.send(f'1|16|{x}|{y}')

    def create_healer_(self, x, y, id,idteam):
        # Création du nexus
        h = buildings.healer(x, y, self, id,idteam,)
        h.update_pos(self.offset_x, self.offset_y)
        if self.fence.check_confirmation(h):
            if h.allyteam:   
                self.nb_buildings += 1
                
            self.sprite_list[id] = h
            if (idteam == 1 and self.ntw.team[0] == 'BLUE') or (idteam == 2 and self.ntw.team[0] == 'RED'):
                self.clickable_list[id] = h
            self.building[h.id] = h

    def move(self, x, y, id): # Envoie l'action au serveur
        self.ntw.send(f'2|{id}|{x}|{y}')

    def move_(self, x, y, id):

        try:
            n = threading.Thread(target=self.sprite_list[int(id)].move, args=((np.array(self.fence.grille), int(float(x)), int(float(y))))).start()
        except:
            pass

    def suppr_sprite(self, id=None, allyteam=False): # Envoie l'action au serveur
        if id == None:
            self.ntw.send(f'3|{self.actived_obj[0].id}|{allyteam}|-1')
        else:
            self.ntw.send(f'3|{id}|{allyteam}|-1')

    def suppr_sprite_(self, id, allyteam):
        try:
            if self.sprite_list[id].allyteam and type(self.sprite_list[id]).__bases__[0].__name__ == "Building": # Si l'objet supprimé est un building allié, le nombre de building diminue
                self.nb_buildings-=1
                       
            if type(self.sprite_list[id]).__bases__[0].__name__ == "Building": # Si l'objet est un bâtiment, libère la position de celui-ci sur la map
                self.fence.update_spots(self.sprite_list[id])

            del self.sprite_list[id] # Supprime l'objet à l'aide de son id
            
            if self.nb_buildings==0: # Envoie un message en fonction du perdant et met fin à la partie (envoie l'action au serveur)
                if self.ntw.team[0] == "BLUE":
                    self.end_of_game("BLUE")
                else:
                    self.end_of_game("RED")
            try:
                self.sprite_list[id].delete_ui() # Esaie de supprimer l'affichage de l'objet
            except:
                pass
            try:
                del self.actived_obj[0].action[0]
            except:
                pass
        except:
            pass

        try:
            del self.clickable_list[int(id)]
        except:
            pass

# LE FONCTIONNEMENT DE LA FONCTION SUIVANTE EST SIMILAIRE A DE NOMBREUSES FONCTIONS QUI SUIVRONT
# UN UNIQUE COMMENTAIRE SERA DONC EFFECTUE (sauf ajout si petit changement, ex: pour la fonction récursive)

    def create_worker_for_the_nexus(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size() # Récupération de la taille de l'écran
        actived_id = self.actived_obj[0].id  # Stockage de l'objet qui fait l'action dans une variable pour pouvoir la modifier même si ce n'est plus l'objet actif en cours
        
        def create_worker_for_the_nexus_():
            # Création de l'unité si la place n'est pas occupée par un bâtiment, la position channge si c'est le cas
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_worker(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_worker(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_worker(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_worker(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_worker(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_worker(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list: # Supprime l'affichage si il est présent sur l'écran
                del self.sprite_list["action_ui_creating_troop"]
                
            del self.sprite_list[actived_id].action_ui[0] # Supprime l'action dans la liste des actions à afficher
            
            if self.actived_obj[0].id == actived_id: # Si l'objet qui effectue l'action est encore celle active; l'affichage est rechargé
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
            
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id) # Ajout d'un bouton qui sera une simple image pour signaler l'action en coours
        self.sprite_list["action_ui_creating_troop"] = self.btn # Affiche le bouton
        self.actived_obj[0].action_ui.append("creating_troop") # Ajoute l'action qui doit être affiché dans la liste
        self.supply += 1

        return create_worker_for_the_nexus_ # Retourne la fonction de création d'objet


# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_workerBis_for_the_nexus(self, _gamemaster, _x, _y):  # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_workerBis_for_the_nexus_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_workerBis(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_workerBis(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_workerBis(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_workerBis(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_workerBis(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_workerBis(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
                
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_workerBis_for_the_nexus_

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_workerTrd_for_the_nexus(self, _gamemaster, _x, _y):  # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id


        def create_workerTrd_for_the_nexus_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_workerTrd(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_workerTrd(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_workerTrd(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_workerTrd(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_workerTrd(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_workerTrd(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.supply += 1
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")

        return create_workerTrd_for_the_nexus_


# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_kamikaze_with_the_producer(self, _gamemaster, _x, _y):  # Création de l'unité 
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_kamikaze_with_the_producer_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_kamikaze(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_kamikaze(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_kamikaze(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_kamikaze(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_kamikaze(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_kamikaze(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_kamikaze_with_the_producer_

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_debuff_with_the_producerBis(self, _gamemaster, _x, _y):  # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_debuff_with_the_producerBis_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_debuff(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_debuff(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_debuff(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_debuff(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_debuff(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_debuff(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_debuff_with_the_producerBis_

# PRESQUE MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_lowcost_with_the_producer(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_lowcost_with_the_producer_():
            def produce(n=0, a=0): # Fonction récursive pour créer l'unité à une position différente de celle qui vient d'être crée
                if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50) and n!=1:
                    _gamemaster.create_lowcost(_x + 140, _y)
                    n=1
                
                elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50) and n!=2:
                    _gamemaster.create_lowcost(_x + 0, _y + 140)
                    n=2

                elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50) and n!=3:
                    _gamemaster.create_lowcost(_x + 140, _y + 140)
                    n=3

                elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50) and n!=4:
                    _gamemaster.create_lowcost(_x - 140, _y)
                    n=4

                elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50) and n!=5:
                    _gamemaster.create_lowcost(_x - 0, _y - 140)
                    n=5

                elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50) and n!=6:
                    _gamemaster.create_lowcost(_x - 140, _y - 140)
                    n=6

                if a==0:
                    produce(n, 1)
                
            produce()

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
            
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 2

        return create_lowcost_with_the_producer_

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_fast_troop_with_the_producer(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_fast_troop_with_the_producer_():
            def produce():
                if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                    _gamemaster.create_fast_troop(_x + 140, _y)
                
                elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                    _gamemaster.create_fast_troop(_x + 0, _y + 140)

                elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                    _gamemaster.create_fast_troop(_x + 140, _y + 140)

                elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                    _gamemaster.create_fast_troop(_x - 140, _y)

                elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                    _gamemaster.create_fast_troop(_x - 0, _y - 140)

                elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                    _gamemaster.create_fast_troop(_x - 140, _y - 140)


            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
            
            self.sprite_list[actived_id].cd = 0
        
            return produce()

        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_fast_troop_with_the_producer_

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_archery_with_the_producer(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_archery_with_the_producer_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_archery(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_archery(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_archery(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_archery(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_archery(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_archery(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_archery_with_the_producer_

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_tank_with_the_producerBis(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_tank_with_the_producerBis_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_tank(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_tank(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_tank(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_tank(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_tank(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_tank(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_tank_with_the_producerBis_
   
   # MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_siege_troop_with_the_producerBis(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_siege_troop_with_the_producerBis_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_siege_troop(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_siege_troop(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_siege_troop(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_siege_troop(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_siege_troop(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_siege_troop(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_siege_troop_with_the_producerBis_
   
   # MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_fast_troop_archery_with_the_producerBis(self, _gamemaster, _x, _y): # Création de l'unité
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        def create_fast_troop_archery_with_the_producerBis_():
            if _gamemaster.fence.check_confirmation_(_x + 140, _y, 50, 50):
                _gamemaster.create_fast_troop_archery(_x + 140, _y)
            
            elif _gamemaster.fence.check_confirmation_(_x + 0, _y + 140, 50, 50):
                _gamemaster.create_fast_troop_archery(_x + 0, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x + 140, _y + 140, 50, 50):
                _gamemaster.create_fast_troop_archery(_x + 140, _y + 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y, 50, 50):
                _gamemaster.create_fast_troop_archery(_x - 140, _y)

            elif _gamemaster.fence.check_confirmation_(_x - 0, _y - 140, 50, 50):
                _gamemaster.create_fast_troop_archery(_x - 0, _y - 140)

            elif _gamemaster.fence.check_confirmation_(_x - 140, _y - 140, 50, 50):
                _gamemaster.create_fast_troop_archery(_x - 140, _y - 140)

            if "action_ui_creating_troop" in self.sprite_list:
                del self.sprite_list["action_ui_creating_troop"]
            del self.sprite_list[actived_id].action_ui[0]
            
            if self.actived_obj[0].id == actived_id:
                self.sprite_list[actived_id].delete_ui()
                self.sprite_list[actived_id].generate_ui()
                
            self.sprite_list[actived_id].cd = 0
        
        self.btn = UI.Button("action_ui_creating_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
        self.sprite_list["action_ui_creating_troop"] = self.btn
        self.actived_obj[0].action_ui.append("creating_troop")
        self.supply += 1

        return create_fast_troop_archery_with_the_producerBis_

# LE FONCTIONNEMENT DE LA FONCTION SUIVANTE EST SIMILAIRE A DE NOMBREUSES FONCTIONS QUI SUIVRONT
# UN UNIQUE COMMENTAIRE SERA DONC EFFECTUE

    def create_upgrader_with_worker(self, x ,y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size() # Récupération de la longueur et largeur
        actived_id = self.actived_obj[0].id # Gardel'id de l'objet en cours pour pouvoir la modifier

        # Vérifie si les ressources et la position du bâtiment à être construit sont disponibles
        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 69,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 67, 150, 150) and self.iron >= self.upgrader_cost[0] and self.gold >= self.upgrader_cost[1]:
            
            def create_upgrader_with_worker_(x_, y_):
                def create(): # Fonction qui crée le bâtiment
                    self.create_upgrader(x_, y_)
                     
                    if "action_ui_building" in self.sprite_list: # Supprime l'affichage si il est visible
                        del self.sprite_list["action_ui_building"]
                        
                    del self.sprite_list[actived_id].action_ui[0] # Supprime l'action qui est sensé être affiché
                    
                    if self.actived_obj[0].id == actived_id: # Si le bâtiment actif est le meme que celui en cours recharge l'affichage
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()
                    
                return create # Retourne la fonction de création
            
            # Bouge le worker en fonction des positions disponibles à partir duquel il pourra construire le bâtiment
            if self.fence.check_confirmation_(x + 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y)

            elif self.fence.check_confirmation_(x + 0, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 150)

            elif self.fence.check_confirmation_(x + 150, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y + 150)

            elif self.fence.check_confirmation_(x - 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y)

            elif self.fence.check_confirmation_(x - 0, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 150)

            elif self.fence.check_confirmation_(x - 150, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y - 150)


            self.btn = UI.Button("action_ui_building", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id) # Création du bouton qui sera une image pour afficher l'action en cours
            self.sprite_list["action_ui_building"] = self.btn # Affiche l'image
             
            self.actived_obj[0].action_ui.append("building") # Ajoute l'action en cours à être affiché

            self.iron -= self.upgrader_cost[0] # Consommation du prix du bâtiment
            
            self.cooldown.append([15, create_upgrader_with_worker_(x, y)]) # Créer le bâtiment avec un cooldown
        
        elif self.iron < self.upgrader_cost[0] or self.gold < self.upgrader_cost[1]: # Affiche le problème de ressource si il a lieu
            self.ressources_issue()
    
    # MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_upgraderBis_with_worker(self, x ,y):# Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 130, 130) and self.iron >= self.upgraderBis_cost[0] and self.gold >= self.upgraderBis_cost[1]:
            def create_upgraderBis_with_worker_(x_, y_):
                def create():
                    self.create_upgraderBis(x_, y_)
                     
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]
                        
                    del self.sprite_list[actived_id].action_ui[0]
                    
                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()
                    
                return create
            
            if self.fence.check_confirmation_(x + 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y)

            elif self.fence.check_confirmation_(x + 0, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 150)

            elif self.fence.check_confirmation_(x + 150, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y + 150)

            elif self.fence.check_confirmation_(x - 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y)

            elif self.fence.check_confirmation_(x - 0, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 150)

            elif self.fence.check_confirmation_(x - 150, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y - 150)

            self.btn = UI.Button("action_ui_building", (w, int(w*502/1535)), 0-w*1/200, h*0.425, "now_building", self, 0, 0, id)
            self.sprite_list["action_ui_building"] = self.btn
             
            self.actived_obj[0].action_ui.append("building")
            
            self.iron -= self.upgraderBis_cost[0]
            self.gold -= self.upgraderBis_cost[1]
            
            self.cooldown.append([15, create_upgraderBis_with_worker_(x, y)])
        
        elif self.iron < self.upgraderBis_cost[0] or self.gold < self.upgraderBis_cost[1]:
            self.ressources_issue()

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_Producer_with_worker(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.producer_cost[0] and self.gold >= self.producer_cost[1]:
            def create_Producer_with_worker_(x_, y_):
                def create():
                    self.create_builder(x_, y_)
                     
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y)

            elif self.fence.check_confirmation_(x + 0, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 150)

            elif self.fence.check_confirmation_(x + 150, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y + 150)

            elif self.fence.check_confirmation_(x - 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y)

            elif self.fence.check_confirmation_(x - 0, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 150)

            elif self.fence.check_confirmation_(x - 150, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y - 150)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
             
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.producer_cost[0]
            self.gold -= self.producer_cost[1]
            
            self.cooldown.append([15, create_Producer_with_worker_(x, y)])

        elif self.iron < self.producer_cost[0] or self.gold < self.producer_cost[1]:
            self.ressources_issue()

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_ProducerBis_with_workerBis(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.producerBis_cost[0] and self.gold >= self.producerBis_cost[1]:
            def create_ProducerBis_with_workerBis_(x_, y_):
                def create():
                    self.create_builder(x_, y_)
                     
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y)

            elif self.fence.check_confirmation_(x + 0, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 150)

            elif self.fence.check_confirmation_(x + 150, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y + 150)

            elif self.fence.check_confirmation_(x - 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y)

            elif self.fence.check_confirmation_(x - 0, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 150)

            elif self.fence.check_confirmation_(x - 150, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y - 150)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
             
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.producerBis_cost[0]
            self.gold -= self.producerBis_cost[1]

            self.cooldown.append([15, create_ProducerBis_with_workerBis_(x, y)])

        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_supply_with_worker(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.supply_cost[0] and self.gold >= self.supply_cost[1]:
            def create_supply_with_worker_(x_, y_):
                def create():
                    self.create_supply(x_, y_)
                    
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y)

            elif self.fence.check_confirmation_(x + 0, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 150)

            elif self.fence.check_confirmation_(x + 150, y + 150, 50, 50):
                self.move(self.actived_obj[0].id, x + 150, y + 150)

            elif self.fence.check_confirmation_(x - 150, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y)

            elif self.fence.check_confirmation_(x - 0, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 150)

            elif self.fence.check_confirmation_(x - 150, y - 150, 50, 50):
                self.move(self.actived_obj[0].id, x - 150, y - 150)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
            
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.supply_cost[0]
            self.gold -= self.supply_cost[1]

            self.cooldown.append([15, create_supply_with_worker_(x, y)])

        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_cannon_with_workerTrd(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.cannon_cost[0] and self.gold >= self.cannon_cost[1] and self.supply + 1 <= self.supply_max:
            def create_cannon_with_workerTrd_(x_, y_):
                def create():
                    self.create_cannon(x_, y_)
                    
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 60, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 60, y)

            elif self.fence.check_confirmation_(x + 0, y + 60, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 60)

            elif self.fence.check_confirmation_(x + 60, y + 60, 50, 50):
                self.move(self.actived_obj[0].id, x + 60, y + 60)

            elif self.fence.check_confirmation_(x - 60, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 60, y)

            elif self.fence.check_confirmation_(x - 0, y - 60, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 60)

            elif self.fence.check_confirmation_(x - 60, y - 60, 50, 50):
                self.move(self.actived_obj[0].id, x - 60, y - 60)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
            
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.cannon_cost[0]
            self.gold -= self.cannon_cost[1]
            self.supply += 1

            self.cooldown.append([15, create_cannon_with_workerTrd_(x, y)])
            
        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()

 # MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE        
    def create_nexus_with_worker(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.nexus_cost[0] and self.gold >= self.nexus_cost[1]:
            def create_nexus_with_workerTrd_(x_, y_):
                def create():
                    self.create_nexus(x_, y_)
                    
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 130, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 130, y)

            elif self.fence.check_confirmation_(x + 0, y + 130, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 130)

            elif self.fence.check_confirmation_(x + 130, y + 130, 50, 50):
                self.move(self.actived_obj[0].id, x + 130, y + 130)

            elif self.fence.check_confirmation_(x - 130, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 130, y)

            elif self.fence.check_confirmation_(x - 0, y - 130, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 130)

            elif self.fence.check_confirmation_(x - 130, y - 130, 50, 50):
                self.move(self.actived_obj[0].id, x - 130, y - 130)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
            
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.nexus_cost[0]
            self.gold -= self.nexus_cost[1]

            self.cooldown.append([15, create_nexus_with_workerTrd_(x, y)])
            
        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()
   
   # MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_healer_with_workerTrd(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.healer_cost[0] and self.gold >= self.healer_cost[1]:
            def create_healer_with_workerTrd_(x_, y_):
                def create():
                    self.create_healer(x_, y_)
                    
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 60, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 60, y)

            elif self.fence.check_confirmation_(x + 0, y + 60, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 60)

            elif self.fence.check_confirmation_(x + 60, y + 60, 50, 50):
                self.move(self.actived_obj[0].id, x + 60, y + 60)

            elif self.fence.check_confirmation_(x - 60, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 60, y)

            elif self.fence.check_confirmation_(x - 0, y - 60, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 60)

            elif self.fence.check_confirmation_(x - 60, y - 60, 50, 50):
                self.move(self.actived_obj[0].id, x - 60, y - 60)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
            
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.healer_cost[0]
            self.gold -= self.healer_cost[1]

            self.cooldown.append([15, create_healer_with_workerTrd_(x, y)])
            
        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()

# MEME FONCTIONNEMENT QUE LA FONCTION PRECEDENTE
    def create_wall_with_workerTrd(self, x, y): # Création du bâtiment
        w, h = pygame.display.get_surface().get_size()
        actived_id = self.actived_obj[0].id

        if self.fence.check_confirmation_(pygame.mouse.get_pos()[0] + self.offset_x - 62,
                                       pygame.mouse.get_pos()[1] + self.offset_y - 60, 140, 140) and self.iron >= self.wall_cost[0] and self.gold >= self.wall_cost[1]:
            def create_wall_with_workerTrd_(x_, y_):
                def create():
                    self.create_wall(x_, y_)
                    
                    if "action_ui_building" in self.sprite_list:
                        del self.sprite_list["action_ui_building"]

                    del self.sprite_list[actived_id].action_ui[0]

                    if self.actived_obj[0].id == actived_id:
                        self.sprite_list[actived_id].delete_ui()
                        self.sprite_list[actived_id].generate_ui()

                return create

            if self.fence.check_confirmation_(x + 130, y, 50, 50):
                self.move(self.actived_obj[0].id, x + 130, y)

            elif self.fence.check_confirmation_(x + 0, y + 130, 50, 50):
                self.move(self.actived_obj[0].id, x + 0, y + 130)

            elif self.fence.check_confirmation_(x + 130, y + 130, 50, 50):
                self.move(self.actived_obj[0].id, x + 130, y + 130)

            elif self.fence.check_confirmation_(x - 130, y, 50, 50):
                self.move(self.actived_obj[0].id, x - 130, y)

            elif self.fence.check_confirmation_(x - 0, y - 130, 50, 50):
                self.move(self.actived_obj[0].id, x - 0, y - 130)

            elif self.fence.check_confirmation_(x - 130, y - 130, 50, 50):
                self.move(self.actived_obj[0].id, x - 130, y - 130)

            self.btn = UI.Button("action_ui_building", (w, int(w * 502 / 1535)), 0, h * 0.425, "now_building", self, 0, 0,
                                id)
            self.sprite_list["action_ui_building"] = self.btn
            
            self.actived_obj[0].action_ui.append("building")

            self.iron -= self.wall_cost[0]
            self.gold -= self.wall_cost[1]

            self.cooldown.append([15, create_wall_with_workerTrd_(x, y)])
            
        elif self.iron < self.producerBis_cost[0] or self.gold < self.producerBis_cost[1]:
            self.ressources_issue()
      
      
    def unlock(self, obj):
        _id = self.actived_obj[0].id # Récupération de l'id de l'objet actif
        def unlock_():   
            def _unlock(): # Fonction qui dévérouille l'unité à dévérouillé
                del self.sprite_list[_id].action_ui[0]

                if "RTS_unlocking_ui" in self.sprite_list: # Supprime l'affichage si il est visible
                    del self.sprite_list["RTS_unlocking_ui"]

                # Dévérouille l'unité dont il est question
                if obj == "low_cost":
                    self.low_cost_unlocked = True
                elif obj == "kamikaze":
                    self.kamikaze_unlocked = True
                elif obj == "fast_troop":
                    self.fast_troop_unlocked = True
                elif obj == "archery":
                    self.archery_unlocked = True
                elif obj == "tank":
                    self.tank_unlocked = True
                elif obj == "fast_troop_archery":
                    self.fast_troop_archery_unlocked = True
                elif obj == "debuff":
                    self.debuff_unlocked = True
                elif obj == "cannon":
                    self.cannon_unlocked = True
                elif obj == "summoner":
                    self.summoner_unlocked = True
                elif obj == "siege_troop":
                    self.siege_troop_unlocked = True
                    
                # Si l'objet activé est un objet dont son affichage change en fonction de l'unité dévérouiillé, l'affichage de celle-ci est rechargé
                if self.actived_obj[0].id == _id or self.actived_obj[0].__class__.__name__ == "Producer" or self.actived_obj[0].__class__.__name__ == "ProducerBis" or self.actived_obj[0].__class__.__name__ == "upgrader" or self.actived_obj[0].__class__.__name__ == "upgraderBis":
                    self.actived_obj[0].delete_ui()
                    self.actived_obj[0].generate_ui()

                self.sprite_list[_id].cd = 0 # Réinitialisation du cooldown

            return _unlock() # Retourne le résultat de la fonction

        def do_it(entity):
            btn_img = UI.Button("RTS_unlocking_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
            self.sprite_list["RTS_unlocking_ui"] = btn_img
             
            self.actived_obj[0].action_ui.append("unlocking")

            # Paye en ressource ce que lui coûte son déblocage
            self.iron -= entity[0]
            self.gold -= entity[1]
            
            self.actived_obj[0].cd = 25000 # Coodown délirant pour ne pas permettre deux déblocages simultanés avec le même objet
        
            self.cooldown.append([20, unlock_])

        if self.actived_obj[0].cd == 0: # Commence l'étape de déblocage d'unités si les ressources du joueur sont suffisantes
            w, h = pygame.display.get_surface().get_size()
            if obj == "low_cost" and self.iron >= self.low_cost_unlock_cost[0] and self.gold >= self.low_cost_unlock_cost[1]:
                do_it(self.low_cost_unlock_cost)
            elif obj == "kamikaze" and self.iron >= self.kamikaze_unlock_cost[0] and self.gold >= self.kamikaze_unlock_cost[1]:
                do_it(self.kamikaze_unlock_cost)
            elif obj == "fast_troop" and self.iron >= self.fast_troop_unlock_cost[0] and self.gold >= self.fast_troop_unlock_cost[1]:
                do_it(self.fast_troop_unlock_cost)
            elif obj == "archery" and self.iron >= self.archery_unlock_cost[0] and self.gold >= self.archery_unlock_cost[1]:
                do_it(self.archery_unlock_cost)
            elif obj == "tank" and self.iron >= self.tank_unlock_cost[0] and self.gold >= self.tank_unlock_cost[1]:
                do_it(self.tank_unlock_cost)
            elif obj == "fast_troop_archery" and self.iron >= self.fast_troop_archery_unlock_cost[0] and self.gold >= self.fast_troop_archery_unlock_cost[1]:
                do_it(self.fast_troop_archery_unlock_cost)
            elif obj == "debuff" and self.iron >= self.debuff_unlock_cost[0] and self.gold >= self.debuff_unlock_cost[1]:
                do_it(self.debuff_unlock_cost)
            elif obj == "cannon" and self.iron >= self.canonn_unlock_cost[0] and self.gold >= self.cannon_unlock_cost[1]:
                do_it(self.canonn_unlock_cost)
            elif obj == "summoner" and self.iron >= self.summoner_unlock_cost[0] and self.gold >= self.summoner_unlock_cost[1]:
                do_it(self.summoner_unlock_cost)
            elif obj == "siege_troop" and self.iron >= self.siege_troop_unlock_cost[0] and self.gold >= self.siege_troop_unlock_cost[1]:
                do_it(self.siege_troop_unlock_cost)
            else: # Si il y a un problème de ressource, cela lui est affiché
                self.ressources_issue()

    def already_unlocked(self):
        def already_unlocked_():
            def do_it(): # Supprime l'affichage d'erreur
                if "already_unlocked" in self.sprite_list:
                    del self.sprite_list["already_unlocked"]
                 
            return do_it()

        w, h = pygame.display.get_surface().get_size()
        # Création de l'image d'erreur en tant que bouton
        btn =  UI.Button("RTS_already_unlocked", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
        self.sprite_list["already_unlocked"] = btn
        self.cooldown.append([3, already_unlocked_]) # Cooldown pour la durée du message d'erreur

    def cannot_unlock(self):
        def cannot_unlock_():
            def do_it():
                if "cannot_unlock" in self.sprite_list: # Supprime l'affichage d'erreur
                    del self.sprite_list["cannot_unlock"]
                 
            return do_it()

        w, h = pygame.display.get_surface().get_size()
        
        # Création de l'image d'erreur en tant que bouton
        btn =  UI.Button("RTS_ununlockable_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
        self.sprite_list["cannot_unlock"] = btn
        self.cooldown.append([3, cannot_unlock_]) # Cooldown pour la durée du message d'erreur

    def No_space(self):
        def No_space_():
            def do_it():
                if "no_space" in self.sprite_list: # Supprime l'affichage d'erreur
                    del self.sprite_list["no_space"]
                 
            return do_it()

        w, h = pygame.display.get_surface().get_size()
        
        # Création de l'image d'erreur en tant que bouton
        btn =  UI.Button("RTS_space_used_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
        self.sprite_list["no_space"] = btn
        self.cooldown.append([6, No_space_]) # Cooldown pour la durée du message d'erreur

# MEME FONCTIONNEMENT QUE PRECEDEMMENT
    def locked_troop(self):
        def locked_troop_():
            def do_it():
                if "locked_troop" in self.sprite_list:
                    del self.sprite_list["locked_troop"]
                 
            return do_it()

        w, h = pygame.display.get_surface().get_size()
        btn =  UI.Button("RTS_locked_troop", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
         
        self.sprite_list["locked_troop"] = btn
        self.cooldown.append([5, locked_troop_])       

# MEME FONCTIONNEMENT QUE PRECEDEMMENT
    def ressources_issue(self):
        def ressources_issue_():
            def do_it():
                if "ressources_issue" in self.sprite_list:
                    del self.sprite_list["ressources_issue"]
            return do_it()

        w, h = pygame.display.get_surface().get_size()
        btn =  UI.Button("RTS_ressources_issue", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)
        self.sprite_list["ressources_issue"] = btn
        self.cooldown.append([6, ressources_issue_])   

    def end_of_game(self, loser): # Envoie au serveur la fin de partie
        if self.ntw.team[0] == 'BLUE':
            self.ntw.send(f'1|42|{loser}')
        else:
            self.ntw.send(f'1|43|{loser}')

    def end_of_game_(self, loser): # Définit le perdant et lance la fin de partie
        self.end_of_game_bol = True
        self.loser = loser
        
    def end_of_game__(self, loser): # Affiche le victoire de défaite ou de victoire en fonction du joueur
        if loser == "BLUE" and self.ntw.team[0] == "BLUE":
            self.graphic.screen.blit(self.img_defeat_blue, [0, 0])  
        elif loser == "BLUE" and self.ntw.team[0] == "RED":
            self.graphic.screen.blit(self.img_victory_red, [0, 0])
        elif loser == "RED" and self.ntw.team[0] == "RED":
            self.graphic.screen.blit(self.img_defeat_red, [0, 0])
        else:
            self.graphic.screen.blit(self.img_victory_blue, [0, 0])  


class Fence:
    def __init__(self):
        # Création de la grille avec une dimension de 10x10
        self.grille = [[1] * 900 for i in range(900)]
        self.case_dimX, self.case_dimY = 10, 10
        self.pos_CaseX = None
        self.pos_CaseY = None
        self.graphic = Graphics()
        
        # Liste des murs de la map 
        self.list_wall = [(5,44),(5,43),(5,42),(5,41),(0,39),(1,39),(2,39),(3,39),(8,40),(8,39),(8,38),(8,37),(8,36),(7,36),(6,36),(5,36),(4,36),(0,32),(1,32),(2,32),(3,32),(4,32),(4,31),(4,30),(13,44),(13,43),(13,42),(13,41),(0,28),(1,28),(2,28),(3,28),(4,28),(5,28),(6,28),(0,27),(1,27),(2,27),(3,27),(4,27),(5,27),(6,27),(6,26),(6,25),(6,24),(6,23),(2,24),(3,24),(4,24),(5,24),(6,24),(7,24),(8,24),(2,23),(0,24),(0,23),(0,22),(0,21),(0,20),(2,21),(2,20),(3,20),(4,20),(5,20),(6,20),(7,20),(8,20),(6,21),(6,20),(6,29),(6,18),(0,17),(1,17),(2,17),(3,17),(4,17),(5,17),(6,17),(0,16),(1,16),(2,16),(3,16),(4,16),(5,16),(6,16),(22,44),(22,43),(22,42),(22,41),(21,41),(23,41),(19,37),(20,37),(21,37),(22,37),(23,37),(24,37),(25,37),(19,36),(19,35),(25,35),(25,36), (31,44),(31,43),(31,42),(31,41),(31,40),(37,43),(38,42),(39,41),(40,40),(41,39),(42,38),(43,37),(43,43),(42,42),(41,41),(40,40),(39,39),(38,38),(37,37),(22,32),(21,31),(20,30),(19,29),(18,28),(16,26),(15,25),(14,24),(13,23),(12,22),(13,21),(14,20),(15,19),(16,18),(18,16),(19,15),(20,14),(21,15),(22,12),(23,13),(24,14),(25,15),(26,26),(28,18),(29,19),(30,20),(31,21),(32,22),(31,23),(30,24),(29,25),(28,26),(26,28),(25,29),(24,30),(23,31),(19,25),(20,25),(21,25),(23,25),(24,25),(25,25),(25,24),(25,23),(25,21),(25,20),(25,19),(24,19),(23,19),(21,19),(20,19),(19,19),(19,20),(19,21),(19,23),(19,24),(39,0),(39,1),(39,2),(39,3),(44,5),(43,5),(42,5),(41,5),(40,8),(39,8),(37,8),(38,8),(37,8),(36,8),(36,7),(36,6),(36,5),(36,4),(44,12),(43,12),(42,12),(42,12),(41,12),(40,12),(40,13),(40,14),(31,0),(31,1),(31,2),(31,3),(44,16),(43,16),(42,16),(41,16),(40,16),(39,16),(38,16),(38,17),(39,17),(40,17),(41,17),(42,17),(43,17),(44,17),(38,18),(38,19),(38,20),(38,21),(36,20),(37,20),(38,20),(39,20),(40,20),(41,21),(44,20),(44,21),(44,22),(44,23),(44,24),(38,23),(38,24),(38,25),(38,26),(36,24),(37,24),(38,24),(39,24),(40,24),(41,24),(38,27),(39,27),(40,27),(41,27),(42,27),(43,27),(44,27),(38,28),(39,28),(40,28),(41,28),(42,28),(43,28),(44,28),(22,0),(22,1),(22,2),(22,3),(21,3),(23,3),(19,7),(20,7),(21,7),(22,7),(23,7),(24,7),(25,7),(19,8),(19,9),(25,8),(25,9),(13,0),(13,1),(13,2),(13,3),(7,1),(6,2),(5,3),(4,4),(3,5),(2,6),(1,7),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7)]
        for i in self.list_wall: # Occupe les cases sur lesquelles sont les murs
            for j in range(21):
                for k in range(21):
                    self.grille[i[1]*20+k-1][i[0]*20+j-1] = 0
    
    def check_confirmation(self, edifice):
        # On divise la position de la souris par la dimension de la case pour trouver son emplacement dans la grille
        mouse_pos = pygame.mouse.get_pos()

        if type(edifice).__bases__[0].__name__ != "Building":
            use = False
        else:
            use = True

        self.pos_CaseX = (edifice.pos_absolu_x) / self.case_dimX
        self.pos_CaseY = (edifice.pos_absolu_y) / self.case_dimY

        # Détermine la longueur pour lesquelles on remplira les cases
        e_w = round(edifice.width / self.case_dimX)
        e_h = round(edifice.height / self.case_dimY)

        # Récupère l'emplacement de x et y dans la grille (arrondissant à l'inférieur)
        coor_x = math.floor((self.pos_CaseX) - e_w / self.case_dimX)
        coor_y = math.floor((self.pos_CaseY) - e_h / self.case_dimY)

        # Verifie que les cases de la grilles soient innocupées dans un certain rayon
        n = 0
        for i in range(e_w):
            if self.grille[coor_y][coor_x + i] == 1:
                for j in range(e_h):
                    if self.grille[coor_y + j][coor_x + i] == 1:
                        n += 1

        # Si toutes les cases sont innocupées, créer le batiment et occupe les cases de la grille dans un certain rayon
        if n == e_w * e_h:
            if use == False:
                return True
            for i in range(e_w):
                self.grille[coor_y][coor_x + i] = 0
                for j in range(e_h):
                    self.grille[coor_y + j][coor_x + i] = 0
            return True
        else:
            return False

    def check_confirmation_(self, pos_absolu_x, pos_absolu_y, width, height):
        # On divise la position de la souris par la dimension de la case pour trouver son emplacement dans la grille
        mouse_pos = pygame.mouse.get_pos()
        self.pos_CaseX = (pos_absolu_x) / self.case_dimX
        self.pos_CaseY = (pos_absolu_y) / self.case_dimY

        # Détermine la longueur pour lesquelles on remplira les cases
        e_w = round(width / self.case_dimX)
        e_h = round(height / self.case_dimY)

        # Récupère l'emplacement de x et y dans la grille (arrondissant à l'inférieur)
        coor_x = math.floor((self.pos_CaseX) - e_w / self.case_dimX)
        coor_y = math.floor((self.pos_CaseY) - e_h / self.case_dimY)

        # Verifie que les cases de la grilles soient innocupées dans un certain rayon
        n = 0
        for i in range(e_w):
            if self.grille[coor_y][coor_x + i] == 1:
                for j in range(e_h):
                    if self.grille[coor_y + j][coor_x + i] == 1:
                        n += 1

        # Si toutes les cases sont innocupées, créer le batiment et occupe les cases de la grille dans un certain rayon
        if n == e_w * e_h:
            return True
        else:
            return False

    def update_spots(self, edifice):
        # On divise la position de la souris par la dimension de la case pour trouver son emplacement dans la grille
        mouse_pos = pygame.mouse.get_pos()
        self.pos_CaseX = (edifice.pos_absolu_x) / self.case_dimX
        self.pos_CaseY = (edifice.pos_absolu_y) / self.case_dimY

        # Détermine la longueur pour lesquelles on remplira les cases
        e_w = round(edifice.width / self.case_dimX)
        e_h = round(edifice.height / self.case_dimY)

        # Récupère l'emplacement de x et y dans la grille (arrondissant à l'inférieur)
        coor_x = math.floor((self.pos_CaseX) - e_w / self.case_dimX)
        coor_y = math.floor((self.pos_CaseY) - e_h / self.case_dimY)

        # Libère les cases de la grille dans un certain rayon
        for i in range(e_w):
            self.grille[coor_y][coor_x + i] = 1
            for j in range(e_h):
                self.grille[coor_y + j][coor_x + i] = 1

    def update(self): # Affiche la grille
        for y in range(0, 1550, self.case_dimY):
            pygame.draw.line(self.graphic.screen, 0xee0000, (0, y), (1550, y))

        for x in range(0, 1600, self.case_dimX):
            pygame.draw.line(self.graphic.screen, 0xee0000, (x, 0), (x, 1600))

