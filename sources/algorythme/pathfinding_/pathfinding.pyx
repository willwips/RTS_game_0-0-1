from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
import algorythme.pathfinding_.Astar as Astar

import ctypes

import threading


class A_star_1(threading.Thread):
    def __init__(self, grid, _start, end):
        threading.Thread.__init__(self)
        self.grid = grid
        self._start = Astar.Node(_start[0], _start[1])

        self.end = Astar.Node(end[0], end[1])

    def run(self):
        try:
            self.path = Astar.A_star(self.grid, self._start, self.end)
        finally:
            pass

    def get_id(self):

        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
                                                         ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')

    def join(self, *args):
        threading.Thread.join(self, *args)
        return self.path


class A_star_2(threading.Thread):
    def __init__(self, grid, _start, end):
        threading.Thread.__init__(self)
        self.grid = Grid(matrix=list(grid))
        self._start = self.grid.node(_start[0], _start[1])
        self.end = self.grid.node(end[0], end[1])
        self.finder = AStarFinder(diagonal_movement=DiagonalMovement.always)

    def run(self):
        try:
            self.path, self.runs = self.finder.find_path(self._start, self.end, self.grid)
        finally:
            pass

    def get_id(self):

        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
                                                         ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')

    def join(self, *args):
        threading.Thread.join(self, *args)
        return self.path


def search_path(_grid, start, end):
    if (start[0]-end[0])**2+(start[1]-end[1])**2<2500:
        t1 = A_star_1(_grid, start, end)
        a = 1
    else:
        t1 = A_star_2(_grid, start, end)
        a = 2
    t1.start()
    if t1.is_alive():
        pass
    else:
        if a == 1:
            return t1.join()[0]
        else:
            return t1.join()

