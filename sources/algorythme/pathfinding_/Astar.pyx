import time
import numpy
import heapq


class Node:
    __slots__ = ('x', 'y', 'cost', 'heuristique', 'parent')

    def __init__(self, x, y, cost=0, heuristique=0, parent=None):
        self.x = x
        self.y = y
        self.cost = cost
        self.heuristique = heuristique
        self.parent = parent

    def __lt__(self, other):
        return self.heuristique < other.heuristique

    def __eq__(self, other):
        return self.heuristique == other.heuristique

    def __str__(self):
        return 'x : ' + str(self.x) + ' y : ' + str(self.y) + ' cost : ' + str(self.cost) + ' heuristique : ' + str(
            self.heuristique) + ' parent : ' + str(self.parent)


def reconstruct_path(node: Node):
    path = []
    while node.parent is not None:
        path.append(node)
        node = node.parent
    path.append(node)

    return path


def distance(n1: Node, n2: Node):
    aaa = time.time_ns()
    a = numpy.sqrt((n1.x - n2.x) ** 2 + (n1.y - n2.y) ** 2) * 2
    b = numpy.sqrt(2) * min(n1.x - n2.x, n1.y - n2.y) + abs(n1.x - n2.x - n1.y - n2.y)
    return a


def A_star(maze: numpy.ndarray, end: Node, start: Node):
    closed_liste: numpy.ndarray[Node] = numpy.array([])
    closed_liste_: list[tuple] = []
    open_list: list[Node] = []
    heapq.heapify(open_list)
    heapq.heappush(open_list, start)
    rr = 0
    b = 0
    c = 0
    d = 0
    e = 0
    f = 0
    h = 0
    print(type(maze))
    while open_list:
        u = heapq.heappop(open_list)
        z = time.time_ns()
        rr += 1
        x = u.x
        y = u.y
        if u.x == end.x and y == end.y:
            return reconstruct_path(u), (d / (rr * 1000) ** 3, e / (rr * 1000 ** 3), h / (rr * 1000 ** 3))
        for i in [(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (-1, 1), (1, -1)]:
            _x = i[0]
            _y = i[1]
            if u.x + _x < 0 or y + _y < 0:
                continue

            g = time.time_ns()

            if (x + _x, y + _y) in closed_liste_:
                h += time.time_ns() - g


            else:
                h += time.time_ns() - g

                c = time.time_ns()
                for j in open_list:
                    if j.x == x + _x and j.y == y + _y and j.cost < u.cost + 1:
                        d += time.time_ns() - c
                        break
                    else:
                        pass
                else:
                    d += time.time_ns() - c
                    f = time.time_ns()
                    try:
                        if maze[y + _y, x + _x] != 1:
                            pass
                        else:
                            node = Node(x + _x, y + _y, cost=u.cost + 1, parent=u)
                            node.heuristique = distance(node, end)
                            heapq.heappush(open_list, node)
                    except IndexError:
                        pass
                    e += time.time_ns() - f

        closed_liste_.append((x, y))
        closed_liste = numpy.append(closed_liste, u)

    raise
