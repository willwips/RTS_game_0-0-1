import os
import socket
import threading
import sys
import time


class Client_Thread(threading.Thread):
    def __init__(self, socket_: socket.socket):
        threading.Thread.__init__(self)
        self.socket = socket_
        self.MSG_LEN = 2048

    def run(self):
        nom = self.name
        # boucle infini qui reçoit des messages et les réenvoie à tout le monde en ajoutant des info supplèmentaire
        while True:
            msgClient = self.socket.recv(self.MSG_LEN).decode('Utf8')
            if not msgClient or msgClient.upper() == 'FIN':
                break
            elif msgClient == 'GETTEAM':
                global team
                message = team
                if team =='BLUE':
                    team = 'RED'
                else:
                    team = 'BLUE'
                while self.name not in connext_client:
                    time.sleep(0.01)
                connext_client[self.name].send(message.encode("Utf8"))
            elif msgClient == 'START':
                for cle in connext_client:

                    connext_client[cle].send('START'.encode("Utf8"))

            else:

                message = msgClient.split('|')
                # permet de gérer lorsque plusieur message sont reçu simultanéent, puis rajoute l'id ainsi que le temps a la quel cette action doit être êffectué
                for i in range(len(message)//5):
                    message_ = message[:5]
                    message_[0] = str(int(float(message_[0])) + TIME_ROUND * 1000000) + '|'
                    message_[1] += '|'
                    message_[2] += '|'
                    message_[3] += '|'
                    message_[4] += '|'


                    global ID
                    message_.append(str(ID))
                    ID += 1
                    message_[5] += '#'

                    message_ = ''.join(message_)
                    for cle in connext_client:
                        connext_client[cle].send(message_.encode("Utf8"))
                    del message[:5]
        self.socket.close()
        del connext_client[nom]
        if len(connext_client) == 0:
            quit()


connext_client = {}
sock  = None
TIME_ROUND = 100

team = 'BLUE'
START = 0
ID = 0

def quit():
    os._exit(0)
def start():
    Host = ''
    Port = 65432
    global sock
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((Host, Port))
    sock.listen(2)


    return (socket.gethostbyname(socket.gethostname()),sock)
def run(sock_):
    # boucle infini permetant de gérer les connexions
    while True:
        # quand une requette de connection est reçu, crée un thread pour communiquer avec le client

        clientsocket, address = sock_.accept()
        client_thread = Client_Thread(clientsocket)
        client_thread.start()
        it = client_thread.name
        connext_client[it] = clientsocket
        print("Client %s connecté, adresse IP %s, port %s." % (it, address[0], address[1]))
