import socket, threading
import time

"""
protocole :


requette divisé en plusieur partie par un |

1ère partie:

1: apparition d'un building/d'un sprite
2: déplacement d'un sprite
3: suppréssion d'un sprite
4: attaque
5: mine

2ème partie



id du sprite: si 1 : nexus 2: worker

2: id du sprite

4: id du sprite

5: id du sprite
3ème partie :

1:

coordonée x d'apparition

2: coordonée x de destination

4: id du sprite attaqué

5: id du minerai
4ème partie:

1:

coordonée y d'apparition


2: coordonée y de destination
"""

class network:
    def __init__(self, Host, Port,gamemaster_):
        self.Host = Host
        self.Port = Port
        self.gamemaster = gamemaster_


        self.socket_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.socket_.connect((self.Host, self.Port))
        except socket.error:
            print("la connexion à échoué")

        self.msg = []
        self.team = []
        self.Emission = Emission(self.socket_)
        self.Th_R = Thread_recpetion(self.socket_, self.msg,self.team,gamemaster_)
        self.Th_R.start()

    def send(self, obj):

        if obj != 'FIN' or obj != 'GETTEAM':

            self.Emission.send(str(time.time_ns()) + '|' + obj+'|')

        else:
            if obj == 'GETTEAM' and len(self.team) >0:
                return
            self.Emission.send('FIN')
class Emission:
    def __init__(self, conn):
        self.connexion = conn

    def send(self, obj):
        self.connexion.send(obj.encode("Utf8"))


class Thread_recpetion(threading.Thread):
    def __init__(self, conn, liste_message,team,gamemaster_):
        threading.Thread.__init__(self)
        self.connexion = conn
        self.liste_message = liste_message
        self.team = team
        self.gamemaster = gamemaster_
    def run(self):
        while True:
            message_recu = self.connexion.recv(1024).decode("Utf8")
            if not message_recu or message_recu.upper() == "FIN":
                break
            elif message_recu == 'BLUE' or message_recu == 'RED':
                self.team.append(message_recu)
            elif message_recu == 'START':

                self.gamemaster.inrun=True
            else:
                message_recu = message_recu.split('#')
                for i in message_recu:
                    if i != '':
                        self.liste_message.append(i)
        print("client arrétée")
        self.connexion.close()