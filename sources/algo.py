
import pyximport
pyximport.install()
from algorythme.pathfinding_.pathfinding import search_path


def pathfinding(_grid, start, end):
    return search_path(_grid, start, end)
