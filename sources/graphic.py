import pygame
import sys

class Graphics:
    def __init__(self):
        # Initializes the necessary attributes and resources for the game graphics.

        pygame.init()
        pygame.display.set_caption("RtsGame")
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.screen.fill((255,255,255))
        self.sprite_group = pygame.sprite.RenderPlain()
        self.sprite_group_on_screen = pygame.sprite.RenderPlain()
    
        self.escape_screen = False
        # self.escape_screen_picture = pygame.transform.scale(pygame.image.load('.../picture/escape_menu.png').convert_alpha(), self.screen.get_size())

        pygame.display.update()

    def draw(self):
        self.sprite_group_on_screen.clear(self.screen, self.clear_callback)
        self.sprite_group_on_screen.draw(self.screen)
        # if e.build_state == True:
        #     g.screen.blit(e.x.image, (pygame.mouse.get_pos()[0]-24, pygame.mouse.get_pos()[1]-20))

        # Affiche le menu si besoin
        if self.escape_screen:
            self.show_esc_menu()


    def clear_callback(self, surf, rect):
        color = 255, 255, 255
        surf.fill(color, rect)

    def load_sprite_on_screen(self):
        pass

    def clear_on_screen_group(self): # Supprime les sprites qui ne doivent pas être affichés
        self.sprite_group_on_screen.empty()

    def add_on_screen_group(self, sprite): # Ajoute les sprites qui doivent être affichés
        self.sprite_group_on_screen.add(sprite)


    
g = Graphics
