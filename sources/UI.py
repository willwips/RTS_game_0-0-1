import math
import time

import pygame
import sys
import gamemaster

id = -11
lfid = -11


class Main_UI(pygame.sprite.Sprite): # Classe (encore un sprite) qui gère tout type d'affichage avec bouton
    def __init__(self, **kwargs): 
        pygame.sprite.Sprite.__init__(self)
        self.gamemaster = kwargs.get('classe', "NoClasse")
        self.action_ui = []
        self.idteam = -1
        self.pos_absolu_x = -500
        self.pos_absolu_y = -500
        self.HP = math.inf
        self.maxHP = math.inf
        
        if self.gamemaster.actived_obj: # Supprime l'affichage en cours si il y en a un
            self.gamemaster.actived_obj[0].delete_ui()
            if self.gamemaster.actived_obj[0].action_ui:
                if self.gamemaster.actived_obj[0].action_ui[0] == "building":
                    if "action_ui_building" in self.gamemaster.sprite_list:
                        del self.gamemaster.sprite_list["action_ui_building"]
                
                elif self.gamemaster.actived_obj[0].action_ui[0] == "creating_troop":
                    if "action_ui_creating_troop" in self.gamemaster.sprite_list:
                        del self.gamemaster.sprite_list["action_ui_creating_troop"]
            
        self.action = []
        self.id = -10
        self.gamemaster.sprite_list[self.id] = self

        self.activated = None
        n = False

        for i in kwargs: 
            if n==False: # Le premier est déjà utilisé pour self.gamemaster
                n=True
            else:
                k = kwargs.get(i, "NoButton")
                global id
                if len(k)!=7: # Créer un bouton avc tous les arguments donnés
                    btn = Button(k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7],id)
                else:
                    btn = Button(k[0], "NoSize", k[1], k[2], k[3], k[4], k[5], k[6],id) # k[0] est une image déjà chargé et dont la taille à été modifié, il n'est pas, nécessaire de modifier sa taille
                self.gamemaster.sprite_list[id] = btn

                if k[4]!=0: # Si il s'agit d'un bouton, cet argument est un str pour effectuer une action et donct clicable, chose non nécessaire pour une image
                    self.gamemaster.clickable_list[id] = btn
                id -= 1

    def update_pos(self, offset_x, offset_y):
        return True

    def delete_ui(self):
        global lfid
        for j in range(id, lfid+1):
            if j in self.gamemaster.sprite_list:
                del self.gamemaster.sprite_list[j]
            if j in self.gamemaster.clickable_list:

                del self.gamemaster.clickable_list[j]

        if self.id in self.gamemaster.sprite_list:
            del self.gamemaster.sprite_list[self.id]

# Méthodes qui retourne un bol pour être définit et donc ne renvoie pas un message d'erreur quand appelée
    def mine(self,id):
        return False
    
    def mine_(self):
        return False

    def _move(self):
        return False
    
    def attack_(self):
        return False
        
class Object_UI(Main_UI): # Menu des unités
    def __init__(self, **kwargs):
        Main_UI.__init__(self, **kwargs)
        w, h = pygame.display.get_surface().get_size()
        self.img = pygame.image.load(f'pictures/sprite/building_option.png').convert_alpha()
        self.image = pygame.transform.scale(self.img, (w, int(w*502/1535)))
        self.rect = self.image.get_rect(topleft=(0, h-int(w*502/1535)))

class Menu_UI(Main_UI): # Menu du joueur
    def __init__(self, **kwargs):
        Main_UI.__init__(self, **kwargs)
        w, h = pygame.display.get_surface().get_size()
        self.img = pygame.image.load(f'pictures/sprite/menu.png').convert_alpha() # Image propre au menu
        self.image = pygame.transform.scale(self.img, (w, h))
        self.rect = self.image.get_rect(topleft=(0, 0))

class Button(pygame.sprite.Sprite):
    def __init__(self, img, size, x, y,type_, _gamemaster,_x,_y,id):
        pygame.sprite.Sprite.__init__(self)
        
        # Emplacement initiale du bouton
        self.pos_absolu_x=-500
        self.pos_absolu_y = -500
        
        if size!="NoSize": # Si l'image n'est pas déjà chargé, la charge en modifiant sa taille
            self.img = pygame.image.load(f'pictures/sprite/{img}.png').convert_alpha()
            self.image = pygame.transform.scale(self.img, size)
        else:
            self.image = img

        # Attributs en fonciton des arguments
        self.rect = self.image.get_rect(topleft=(x, y))
        self.type = type_
        self.gamemaster = _gamemaster
        self._x = _x
        self._y = _y
        self.id = id
        self.activated = False
        self.idteam = -1
        self.HP = math.inf
        self.maxHP = math.inf
        
    def update_pos(self, offset_x, offset_y):
        return True

    def click(self, pos, n = True):
        # Vérifie que le batiment soit cliqué, si oui effectue la fonction suivante
        if self.rect.collidepoint(pos) and self.type !=0:

            if n:
                self.on_click()
            return True
        return False
    
    def attack_(self):
        return False
    
    def on_click(self):
        # Bouton qui effectue une action en fonction de son type
            
        if self.gamemaster.actived_obj: # Récupère l'id de l'objet actif si il y en a un
            actived_id = self.gamemaster.actived_obj[0].id

# CE TYPE D'ACTION N'EST COMMENTEE QU'UNE SEULE FOIS, SI BESOIN D'INFORMATION POUR LES ACTIONS SIMILAIRE, REVOIR CE COMENTAIRE
        if self.type == 'create_worker': # Si le type d'action est le suivant
            if self.gamemaster.sprite_list[actived_id].cd == 0: # Vérifie que le cooldown de l'objet actif soit de 0
                # Si le coût de l'objet est inférieur aux ressources du joueur
                if self.gamemaster.iron >= self.gamemaster.worker_cost[0] and self.gamemaster.gold >= self.gamemaster.worker_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max and self.gamemaster.worker+1 <= self.gamemaster.max_worker:
                    self.gamemaster.iron -= self.gamemaster.worker_cost[0] # Paye le coût
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_worker_for_the_nexus(self.gamemaster, self._x, self._y)]) # Met en attente la création de l'unité
                    self.gamemaster.sprite_list[actived_id].cd = 150 # Initialise un nouveau cooldown pour que l'action ne soit pas répété de suite
                elif self.gamemaster.iron < self.gamemaster.worker_cost[0] or self.gamemaster.gold < self.gamemaster.worker_cost[1]: # En cas de ressources insuffisantes, un message d'erreur apparait
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_workerBis':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.workerBis_cost[0] and self.gamemaster.gold >= self.gamemaster.workerBis_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.workerBis_cost[0]
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_workerBis_for_the_nexus(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif self.gamemaster.iron < self.gamemaster.workerBis_cost[0] or self.gamemaster.gold < self.gamemaster.workerBis_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_workerTrd':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.workerTrd_cost[0] and self.gamemaster.gold >= self.gamemaster.workerTrd_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.workerTrd_cost[0]
                    self.gamemaster.gold -= self.gamemaster.workerTrd_cost[1]

                    self.gamemaster.cooldown.append([5, self.gamemaster.create_workerTrd_for_the_nexus(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif self.gamemaster.iron < self.gamemaster.workerTrd_cost[0] or self.gamemaster.gold < self.gamemaster.workerTrd_cost[1]:
                    self.gamemaster.ressources_issue()

# CHANGEMENT CONCERNANT UN MESSAGE D'ERREUR
        elif self.type=='create_lowcost':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.low_cost_cost[0] and self.gamemaster.low_cost_unlocked and  self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.low_cost_cost[0]
                    self.gamemaster.cooldown.append(
                        [5, self.gamemaster.create_lowcost_with_the_producer(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.low_cost_unlocked: # Si l'unité n'est pas développé, affiche un message d'erreur
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.low_cost_cost[0]:
                    self.gamemaster.ressources_issue()

        elif self.type=='create_fast_troop':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.fast_troop_cost[0] and self.gamemaster.fast_troop_unlocked and  self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.fast_troop_cost[0]
                    self.gamemaster.cooldown.append(
                        [5, self.gamemaster.create_fast_troop_with_the_producer(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.fast_troop_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.fast_troop_cost[0]:
                    self.gamemaster.ressources_issue()

        elif self.type=='create_kamikaze':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.kamikaze_cost[0] and self.gamemaster.gold >= self.gamemaster.kamikaze_cost[1] and self.gamemaster.kamikaze_unlocked and self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.kamikaze_cost[0]
                    self.gamemaster.gold -= self.gamemaster.kamikaze_cost[1]
                    self.gamemaster.cooldown.append(
                        [5, self.gamemaster.create_kamikaze_with_the_producer(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.kamikaze_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.kamikaze_cost[0] or self.gamemaster.gold < self.gamemaster.kamikaze_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type=='create_archery':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.archery_cost[0] and self.gamemaster.gold >= self.gamemaster.archery_cost[1] and self.gamemaster.archery_unlocked and self.gamemaster.supply + 1 <= self.gamemaster.supply_max:
                    self.gamemaster.iron -= self.gamemaster.archery_cost[0]
                    self.gamemaster.gold -= self.gamemaster.archery_cost[1]
                    self.gamemaster.cooldown.append(
                        [5, self.gamemaster.create_archery_with_the_producer(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.archery_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.archery_cost or self.gamemaster.gold < self.gamemaster.archery_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_tank':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.tank_cost[0] and self.gamemaster.gold >= self.gamemaster.tank_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max and self.gamemaster.tank_unlocked:
                    self.gamemaster.iron -= self.gamemaster.tank_cost[0]
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_tank_with_the_producerBis(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.tank_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.tank_cost[0] or self.gamemaster.gold < self.gamemaster.tank_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_siege_troop':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.siege_troop_cost[0] and self.gamemaster.gold >= self.gamemaster.siege_troop_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max and self.gamemaster.siege_troop_unlocked:
                    self.gamemaster.iron -= self.gamemaster.siege_troop_cost[0]
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_siege_troop_with_the_producerBis(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.siege_troop_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.siege_troop_cost[0] or self.gamemaster.gold < self.gamemaster.siege_troop_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_fast_troop_archery':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.fast_troop_archery_cost[0] and self.gamemaster.gold >= self.gamemaster.fast_troop_archery_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max and self.gamemaster.fast_troop_archery_unlocked:
                    self.gamemaster.iron -= self.gamemaster.fast_troop_archery_cost[0]
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_fast_troop_archery_with_the_producerBis(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.fast_troop_archery_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.fast_troop_archery_cost[0] or self.gamemaster.gold < self.gamemaster.fast_troop_archery_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == 'create_debuff':
            if self.gamemaster.sprite_list[actived_id].cd == 0:
                if self.gamemaster.iron >= self.gamemaster.debuff_cost[0] and self.gamemaster.gold >= self.gamemaster.debuff_cost[1] and self.gamemaster.supply + 1 <= self.gamemaster.supply_max and self.gamemaster.debuff_unlocked:
                    self.gamemaster.iron -= self.gamemaster.debuff_cost[0]
                    self.gamemaster.cooldown.append([5, self.gamemaster.create_debuff_with_the_producerBis(self.gamemaster, self._x, self._y)])
                    self.gamemaster.sprite_list[actived_id].cd = 150
                elif not self.gamemaster.debuff_unlocked:
                    self.gamemaster.locked_troop()
                elif self.gamemaster.iron < self.gamemaster.debuff_cost[0] or self.gamemaster.gold < self.gamemaster.debuff_cost[1]:
                    self.gamemaster.ressources_issue()

        elif self.type == "delete_building":
            self.gamemaster.actived_obj[0].death()

# POUR LE DEBLOCAGE DE'UNITE, IL S'AGIT TOUJOURS DE LA MÊME CHOSE, REVOIR CE COMMENTAIRE SI NECESSAIRE
        elif self.type == "unlock_unité_pas_cher": # Si le type de bouton est le suivant
            if not self.gamemaster.actived_obj[0].action_ui: # # Si aucune action n'est en cours (Vérifié par l'existance d'un affichage prpore à une action)
                if not self.gamemaster.low_cost_unlocked: # Si l'unité n'est pas débloquer, la débloque
                    self.gamemaster.unlock("low_cost")
                else: # Affiche un message d'erreur si l'unité est déjà débloqué
                    self.gamemaster.already_unlocked()
        
        elif self.type == "unlock_kamikaze":
            if not self.gamemaster.actived_obj[0].action_ui:
                if not self.gamemaster.kamikaze_unlocked and self.gamemaster.low_cost_unlocked:
                    self.gamemaster.unlock("kamikaze")
                elif not self.gamemaster.low_cost_unlocked:
                    self.gamemaster.cannot_unlock()
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_fast_troop":
            if not self.gamemaster.actived_obj[0].action_ui:
                if not self.gamemaster.fast_troop_unlocked:
                    self.gamemaster.unlock("fast_troop")
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_archery":
            if not self.gamemaster.actived_obj[0].action_ui:
                if not self.gamemaster.archery_unlocked and self.gamemaster.fast_troop_unlocked:
                    self.gamemaster.unlock("archery")
                elif not self.gamemaster.fast_troop_unlocked:
                    self.gamemaster.cannot_unlock()
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_tank":
            if not self.gamemaster.actived_obj[0].action_ui:
                if not self.gamemaster.tank_unlocked:
                    self.gamemaster.unlock("tank")
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_fast_troop_archery":
            if not self.gamemaster.actived_obj[0].action_ui:
                if not self.gamemaster.fast_troop_archery_unlocked:
                    self.gamemaster.unlock("fast_troop_archery")
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_debuff":
            if not self.gamemaster.actived_obj[0].action_ui:
                if self.gamemaster.fast_troop_archery_unlocked and not self.gamemaster.debuff_unlocked:
                    self.gamemaster.unlock("debuff")
                elif not self.gamemaster.fast_troop_archery_unlocked:
                    self.gamemaster.cannot_unlock()
                else:
                    self.gamemaster.already_unlocked()

        elif self.type == "unlock_siege_troop":
            if not self.gamemaster.actived_obj[0].action_ui:
                if self.gamemaster.tank_unlocked and not self.gamemaster.siege_troop_unlocked:
                    self.gamemaster.unlock("siege_troop")
                elif not self.gamemaster.tank_unlocked:
                    self.gamemaster.cannot_unlock()
                else:
                    self.gamemaster.already_unlocked()

# POUR LA CREATION DE BÂTIMENT, IL S'AGIT TOUJOURS DE LA MÊME CHOSE, REVOIR CE COMMENTAIRE SI NECESSAIRE
        elif self.type == 'create_cannon': # Si le type de bouton est le suivant
            if not self.gamemaster.actived_obj[0].action_ui: # Si aucune action n'est en cours (Vérifié par l'existance d'un affichage prpore à une action)
                self.gamemaster.actived_obj[0].action.append("create_cannon")

        elif self.type == 'create_upgrader':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_upgrader")

        elif self.type == 'create_wall':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_wall")

        elif self.type == 'create_upgraderBis':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_upgraderBis")

        elif self.type == 'create_Producer':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_Producer")

        elif self.type == 'create_ProducerBis':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_ProducerBis")

        elif self.type == 'create_nexus':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_nexus")
        
        elif self.type == 'create_supply':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_supply")

        elif self.type == 'create_healer':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_healer")
        
        elif self.type == 'create_supply':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("create_supply")
                
        # Ajoute l'action de fusion au joueur
        elif self.type == 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON':
            self.gamemaster.actived_obj[0].action.append("FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON")

        elif self.type == 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS':
            self.gamemaster.actived_obj[0].action.append("FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS")

        elif self.type == 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD':
            self.gamemaster.actived_obj[0].action.append("FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD")

        # Ajoute l'action de minage au joueur
        elif self.type == 'mine':
            if not self.gamemaster.actived_obj[0].action_ui:
                self.gamemaster.actived_obj[0].action.append("mine")            

        elif self.type == 'boom': 
            # Fait exploser l'unité: la détruit en faisant des dégats aux unités dans une certaine distance
            for i in list(self.gamemaster.sprite_list.values()): 
                if (not ((i.idteam == 1 and self.gamemaster.ntw.team[0] == 'BLUE') or (i.idteam == 2 and self.gamemaster.ntw.team[0] == 'RED')) and ((self._x-i.pos_absolu_x)**2+(self._y-i.pos_absolu_y)**2)**(1/2) < 150) or ((self._x-i.pos_absolu_x)**2+(self._y-i.pos_absolu_y)**2)**(1/2) == 0:
                            i.HP -= 25
                            if i.HP <= 0: i.death()
        
    
        elif self.type == "get_menu":
            w, h = pygame.display.get_surface().get_size()
            if self.activated == False: # Si le menu n'est pas affiché, l'affiche avec différents boutons
                self.menu = Menu_UI(classe=self.gamemaster, btn1=("btn_menu_REPRENDRE", (w*0.165, h*0.0754), w*0.4175, h*0.211, "get_menu_optn_reprendre", self.gamemaster, 0, 0, id), btn2=("RTS_give_up", (w*0.165, h*0.0754), w*0.4175, h*0.35, "give_up", self.gamemaster, 0, 0, id), btn3=("btn_menu_AIDE", (w*0.165, h*0.0754), w*0.4175, h*0.485, "get_menu_optn_aide", self.gamemaster, 0, 0, id), btn4=("btn_menu_QUITTER", (w*0.165, h*0.0754), w*0.4175, h*0.622, "get_menu_optn_quitter", self.gamemaster, 0, 0, id))
                self.gamemaster.actived_obj.append(self.menu)
                self.activated = True
            else: # Si le menu est affiché, le supprime
                self.menu.delete_ui()
                self.activated = False

        elif self.type == "get_menu_optn_aide":
            w, h = pygame.display.get_surface().get_size()
            if self.activated == False: # Affiche une image d'aide
                self.menu_aide = Menu_UI(classe=self.gamemaster, btn1=("RTS_img_aide", (w, h), 0, 0, "", self.gamemaster, 0, 0, id))
                self.gamemaster.actived_obj.append(self.menu_aide)
                self.activated = True
            else:
                self.menu_aide.delete_ui()
                self.activated = False

        elif self.type == "give_up":
            if self.gamemaster.ntw.team[0] == "BLUE":  # Met fin à la partie en déclarant pour perdant le joueur qui abandone 
                self.gamemaster.end_of_game("BLUE")
                
            else:
                self.gamemaster.end_of_game("RED")

        elif self.type == "get_menu_optn_reprendre": 
            self.gamemaster.actived_obj[0].delete_ui() # Supprime l'affichage du menu
            self.activated = False

        elif self.type == "get_menu_optn_quitter": # Quitte le jeu
            self.gamemaster.ntw.send('FIN')
            pygame.quit()
            sys.exit()

        self.on_click_ = True

    def mine(self,id):
        return False
    
    def mine_(self):
        return False
    
    def _move(self):
        return False
    
 