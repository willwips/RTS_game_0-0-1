
from tkinter import *
import server
import main
import multiprocessing as mp

class graphique(Tk):
    #classe permétant de crée et gérer la fenètre de mon launcher

    def __init__(self):
        # creation de la fenetre et des widgets
        self.gamemaster = None
        self.serv = False
        super().__init__()

        self.title('Launcher')
        self.geometry('450x300')

        self.a = None
        self.label = Label(self, text= 'Id du serveur     :', font = ('', 14))
        self.label.place(x=10, y = 10)

        self.entrée1 = Entry(width=30)
        self.entrée1.place(y = 15, x = 210)
    
        self.button = Button(self, text = "Rejoindre serveur", font=('', 14))
        self.button.place( x = 170, y = 50)
        self.button['command'] = self.join_server

        self.button = Button(self, text = "Créer serveur", font=('', 14))

        self.button.place( x = 170, y = 90)
        self.button['command'] = self.create_server

        self.button = Button(self, text = "Start", font=('', 14))

        self.button.place( x = 170, y = 140)
        self.button['command'] = self.start
        self.log = Label(self, text= '', font = ('', 14))
        self.log.place(x=0, y = 270)

    def create_server(self):
        # fonction permétant de lancer un serveur
        ip, sock = server.start()
        self.serv = True
        self.a = mp.Process(target = server.run, args = (sock,))
        self.a.start()
        self.log.config(text=f'Vous avez crée un serveur a l\'ip : {str(ip)}')

    def join_server(self):
        # fonction permétant de rejoindre un serveur

        self.gamemaster = main.connectserv(str(self.entrée1.get()))
        self.log.config(text=f'Vous êtes  connécté au serveur a l\'ip : {str(self.entrée1.get())}')

    def start(self):
        # fonction permétant de lancer une partie
        self.log.config(text=f'en attente du serveur')
        self.destroy()
        try:
            main.run(self.gamemaster)
        except:
            pass


def launch():
    g = graphique()
    g.mainloop()


if __name__ == '__main__':
    launch()