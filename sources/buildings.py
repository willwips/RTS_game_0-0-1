import math
import random

import pygame
import gamemaster
import UI
from graphic import *


class map_(pygame.sprite.Sprite): # Initialisation de l'objet map en tant que sprite
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.map = pygame.transform.scale(pygame.image.load('pictures/map/map.png').convert_alpha(),(9000,9000)) # Chargement de l'image et modification de sa taille
       
       # Déclaration d'attributs
        self.id = -2
        self.idteam = -1

        # Attribue l'image de l'objet ainsi que son rectangle, sa taille
        self.image = self.map
        self.rect = self.image.get_rect()

        # Initialisation de la position du sprite
        self.pos_absolu_x = self.rect.x = 0
        self.pos_absolu_y = self.rect.y = 0

        # Liste qui récupère les chemins
        self._path = []

        # Cible correspondant aux coordonnées
        self.target_x = self.pos_absolu_x
        self.target_y = self.pos_absolu_y

        # Autres attributs
        self.offscreen = False
        self.HP = math.inf
        self.maxHP = math.inf

    def update_pos(self, offset_x, offset_y):
        # on met à jour la position du rectangle en fonction de la caméra
        self.rect.x = self.pos_absolu_x - offset_x
        self.rect.y = self.pos_absolu_y - offset_y
        # on voit si le sprite est en dehors de l'écran
        return True

# Fonction qui retourne toutes False, necessaire car c'est un sprite et a donc des actions similaires aux autres. Même principe pour d'autres classes
    def mine(self, id):
        return False

    def mine_(self):
        return False

    def _move(self):
        return False

    def attack_(self):
        return False


class Building(pygame.sprite.Sprite): # Classe qui est un sprite
    def __init__(self, HP, img, size, coord_x, coord_y, width, height, _gamemaster, id, allyteam, ac=0, reach=0, ):
        pygame.sprite.Sprite.__init__(self)

        # Initilisation des variables communes à tous les bâtiments
        self.gamemaster = _gamemaster
        self.img = pygame.image.load(f'pictures/sprite/{img}').convert_alpha()
        self.image = pygame.transform.scale(self.img, size)
        self.rect = self.image.get_rect()
        self.HP = HP
        self.ac = ac
        self.reach = reach
        self.rect.x = coord_x
        self.rect.y = coord_y
        self.pos_absolu_x = coord_x
        self.pos_absolu_y = coord_y
        self.allyteam = allyteam

        self._path = []

        self.action = []
        self.action_ui = []

        self.cd = 0
        self.maxHP= HP
        self.offscreen = False

        self.width = width
        self.height = height
        
        
        if self.allyteam: # Si le bâtiment est un bâtiment allié au joueur, le brouillard diminue
            for i in range(int(self.pos_absolu_x / 10) - 10, int(self.pos_absolu_x / 10) + 11):
                for j in range(int(self.pos_absolu_y / 10) - 10, int(self.pos_absolu_y / 10) + 11):
                    try:
                        self.gamemaster.brouillard_grille[i][j] = 1
                    except:
                        pass

        self.id = id

    def attack_(self):
        return False

    def click(self, pos, n=True):
        # Vérifie que le batiment soit cliqué, si oui effectue la fonction suivante
        if self.rect.collidepoint(pos):
            if self.gamemaster.actived_obj and n:
                if self.gamemaster.actived_obj[0].action:
                    del self.gamemaster.actived_obj[0].action[0]
            if n:
                self.on_click()
            return True
        return False

    def mine(self, id):
        return False

    def mine_(self):
        return False

    def on_click(self):
        self.on_click_ = True
        # si un élément est actif et est différent à celui qui vient d'être activé, il est remplacé
        # si aucun élément n'est activé, celui-ci s'active
        if len(self.gamemaster.actived_obj) == 1:
            if self.gamemaster.actived_obj[0] != self:
                self.gamemaster.actived_obj[0].delete_ui()
                del self.gamemaster.actived_obj[0]
                self.generate_ui()
        else:
            self.generate_ui()

    def update_pos(self, offset_x, offset_y):
        # on met à jour le cooldown si il y en a un
        if self.cd != 0:
            self.cd -= 1

        # on met à jour la position du rectangle en fonction de la caméra
        self.rect.x = self.pos_absolu_x - offset_x
        self.rect.y = self.pos_absolu_y - offset_y
        # on voit si le sprite est en dehors de l'écran
        if (self.rect.x < 0 or self.rect.x > pygame.display.get_surface().get_size()[
            0] or self.rect.y < 0 or self.rect.y > pygame.display.get_surface().get_size()[0]) or \
                self.gamemaster.brouillard_grille[int(self.pos_absolu_x / 10)][int(self.pos_absolu_y / 10)] == 0:
            return False
        return True

    def move(self):
        return False

    def _move(self):
        return False

    def death(self): # Supprime le sprite
        self.delete_ui()
        self.gamemaster.suppr_sprite(self.id, self.allyteam)

    def locked_color(self, img): # La couleur de l'image devient noir
        w, h = img.get_size()
        for x in range(w):
            for y in range(h):
                a = img.get_at((x, y))[3]
                img.set_at((x, y), pygame.Color(0, 0, 0, a))


class nexus(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        # Initialisation de l'objet en fonction de son équipe
        if idteam == 1:
            Building.__init__(self, 1000, "RTS_Nexus_blue.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 1000, "RTS_Nexus_red.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        # Attributs
        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam
        
        if self.allyteam: # Augmente la taille du nombre maximum de bâtiments si batiment allié
            self.gamemaster.supply_max += 5

    def death(self): # Mort du sprite, suppression de celui-ci
        if self.allyteam:
            self.gamemaster.supply_max -= 5 # Réduit la taille du nombre maximum de bâtiments si batiment allié

        self.gamemaster.actived_obj[0].delete_ui()
        self.gamemaster.suppr_sprite(self.id, self.allyteam)

# TOUTES LES FONCTIONS GENERATE_UI SONT CONSTRUITES DE LA MEME MANIERE, LE COMMENTAIRE DE CE CODE S'APPLIQUE DONC AUX AUTRES (LES MODIFICATIONS SERONT COMMENTES SI IL Y EN A)
    def generate_ui(self): # Génère l'affiche de l'unité en fonction de l'équipe du joueur et de l'action de ce bâtiment
        w, h = pygame.display.get_surface().get_size()
        if self.idteam == 1:
            if self.action_ui: # Si une action en cours qui doit être affichée existe, change l'affichage du building
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                    "RTS_Nexus_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0),
                                                     image2=(
                                                     "RTS_Nexus_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0,
                                                     self.gamemaster, 0, 0, 0, 0), image3=(
                        "RTS_create_units_ui", (w, int(w * 502 / 1535)), 0, h - int(w * 502 / 1535), 0, self.gamemaster,
                        0, 0, 0, 0),
                        btn3=(
                        "RTS_workerBis_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.2, h * 0.923, 'create_workerBis',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), 
                        btn4=(
                        "RTS_workerTrd_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.268, h * 0.923, 'create_workerTrd',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),   
                        btn1=(
                        "RTS_Worker_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.132, h * 0.923, 'create_worker',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), 
                        btn2=(
                        "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), 
                        img_action=(
                        "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0 - w * 1 / 200, h * 0.425, 0,
                        self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                "RTS_Nexus_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
                "RTS_Nexus_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0),
                                                 image3=("RTS_create_units_ui", (w, int(w * 502 / 1535)), 0,
                                                         h - int(w * 502 / 1535), 0, self.gamemaster, 0, 0, 0, 0),
                                                 btn3=(
                                                 "RTS_workerBis_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.2, h * 0.923,
                                                 'create_workerBis', self.gamemaster, self.pos_absolu_x,
                                                 self.pos_absolu_y), btn1=(
                    "RTS_Worker_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.132, h * 0.923, 'create_worker',
                    self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), 
                    btn4=(
                    "RTS_workerTrd_blue", (w * 7 / 160, h * 1 / 13.2), w * 0.268, h * 0.923, 'create_workerTrd',
                    self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),   
                    btn2=(
                    "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building',
                    self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                    "RTS_Nexus_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0),
                                                     image2=(
                                                     "RTS_Nexus_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0,
                                                     self.gamemaster, 0, 0, 0, 0), image3=(
                        "RTS_create_units_ui", (w, int(w * 502 / 1535)), 0, h - int(w * 502 / 1535), 0, self.gamemaster,
                        0, 0, 0, 0), btn1=(
                        "RTS_Worker_red", (w * 7 / 160, h * 1 / 13.2), w * 0.132, h * 0.923, 'create_worker',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=(
                        "RTS_WorkerBis_red", (w * 7 / 160, h * 1 / 13.2), w * 0.2, h * 0.923, 'create_workerBis',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), 
                        btn4=(
                        "RTS_workerTrd_red", (w * 7 / 160, h * 1 / 13.2), w * 0.268, h * 0.923, 'create_workerTrd',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),  
                        btn2=(
                        "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building',
                        self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action=(
                        "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0 - w * 1 / 200, h * 0.425, 0,
                        self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                "RTS_Nexus_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
                "RTS_Nexus_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image3=(
                "RTS_create_units_ui", (w, int(w * 502 / 1535)), 0, h - int(w * 502 / 1535), 0, self.gamemaster, 0, 0,
                0, 0), btn1=(
                "RTS_Worker_red", (w * 7 / 160, h * 1 / 13.2), w * 0.132, h * 0.923, 'create_worker', self.gamemaster,
                self.pos_absolu_x, self.pos_absolu_y), 
                btn4=(
                "RTS_workerTrd_red", (w * 7 / 160, h * 1 / 13.2), w * 0.268, h * 0.923, 'create_workerTrd',
                self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),  
                btn3=(
                "RTS_WorkerBis_red", (w * 7 / 160, h * 1 / 13.2), w * 0.2, h * 0.923, 'create_workerBis',
                self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=(
                "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
                self.pos_absolu_x, self.pos_absolu_y))
       
       # Ajoute l'objet à la liste d'objet qui est activé
        self.gamemaster.actived_obj = []
        self.gamemaster.actived_obj.append(self)

    def delete_ui(self): # Supprime l'affichage de l'objet
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS, VOIR SON COMMMENTAIRE SI BESOIN
class supply(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 350, "RTS_supply_blue.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 350, "RTS_supply_red.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam
        if self.allyteam:
            self.gamemaster.supply_max += 7

    def death(self):
        if self.allyteam:
            self.gamemaster.supply_max -= 7
        self.delete_ui()
        self.gamemaster.suppr_sprite(self.id, self.allyteam)

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        if self.idteam == 1:

            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                "RTS_supply_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, 0, 0, 0, 0, 0), image2=(
                "RTS_supply_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, 0, 0, 0, 0, 0))
        else:

            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                "RTS_supply_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, 0, 0, 0, 0, 0), image2=(
                "RTS_supply_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, 0, 0, 0, 0, 0))
        self.gamemaster.actived_obj = []
        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# PRESQUE MEME FONCTIONNEMENT QUE LA CLASSE NEXUS, VOIR COMMMENTAIRE DU NEXUS SI BESOIN (les changements sont commentés)
class upgrader(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 600, "RTS_Upgrader_blue.png", (135, 135), x, y, 150, 150, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 600, "RTS_Upgrader_red.png", (135, 135), x, y, 150, 150, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam

        w, h = pygame.display.get_surface().get_size()

        # Chargement des images en fonction de l'équipe au préalable pour pouvoir la modifier avant de créer les boutond pour l'affichage de l'unité
        if self.idteam == 1:
            self.img_kamikaze = pygame.image.load('pictures/sprite/RTS_kamikaze_blue.png').convert_alpha()
            self.img_kamikaze = pygame.transform.scale(self.img_kamikaze, (w * 7 / 160, h * 1 / 13.2))
            self.kamikaze_img = pygame.image.load('pictures/sprite/RTS_kamikaze_blue.png').convert_alpha()
            self.kamikaze_img = pygame.transform.scale(self.kamikaze_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_archery = pygame.image.load('pictures/sprite/RTS_archery_blue.png').convert_alpha()
            self.img_archery = pygame.transform.scale(self.img_archery, (w * 7 / 160, h * 1 / 13.2))
            self.archery_img = pygame.image.load('pictures/sprite/RTS_archery_blue.png').convert_alpha()
            self.archery_img = pygame.transform.scale(self.archery_img, (w * 7 / 160, h * 1 / 13.2))
        else:
            self.img_kamikaze = pygame.image.load('pictures/sprite/RTS_kamikaze_red.png').convert_alpha()
            self.img_kamikaze = pygame.transform.scale(self.img_kamikaze, (w * 7 / 160, h * 1 / 13.2))
            self.kamikaze_img = pygame.image.load('pictures/sprite/RTS_kamikaze_red.png').convert_alpha()
            self.kamikaze_img = pygame.transform.scale(self.kamikaze_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_archery = pygame.image.load('pictures/sprite/RTS_archery_red.png').convert_alpha()
            self.img_archery = pygame.transform.scale(self.img_archery, (w * 7 / 160, h * 1 / 13.2))
            self.archery_img = pygame.image.load('pictures/sprite/RTS_archery_red.png').convert_alpha()
            self.archery_img = pygame.transform.scale(self.archery_img, (w * 7 / 160, h * 1 / 13.2))

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()

        # Si les éléments ne sont pas débloqués, change la couleur de l'image de ceux-ci en noir, sinon réinitialise l'image coloré avec son apparence initiale
        if not self.gamemaster.low_cost_unlocked:
            self.locked_color(self.img_kamikaze)
        else:
            self.img_kamikaze = self.kamikaze_img

        if not self.gamemaster.fast_troop_unlocked:
            self.locked_color(self.img_archery)
        else:
            self.img_archery = self.archery_img

        if self.idteam == 1:
            if self.action_ui:
                if self.action_ui[0]=="unlocking":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Upgrader_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Upgrader_blue", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_lowcost_blue", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_unité_pas_cher', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn2=(self.img_kamikaze, w*0.217, h*0.923, 'unlock_kamikaze', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_fast_troop_blue", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=(self.img_archery, w*0.353, h*0.923, 'unlock_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_ui=("RTS_unlocking_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Upgrader_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Upgrader_blue", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_lowcost_blue", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_unité_pas_cher', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn2=(self.img_kamikaze, w*0.217, h*0.923, 'unlock_kamikaze', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_fast_troop_blue", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=(self.img_archery, w*0.353, h*0.923, 'unlock_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:
                if self.action_ui[0]=="unlocking":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Upgrader_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Upgrader_red", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_lowcost_red", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_unité_pas_cher', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn2=(self.img_kamikaze, w*0.217, h*0.923, 'unlock_kamikaze', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_fast_troop_red", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=(self.img_archery, w*0.353, h*0.923, 'unlock_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_ui=("RTS_unlocking_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Upgrader_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Upgrader_red", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_lowcost_red", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_unité_pas_cher', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn2=(self.img_kamikaze, w*0.217, h*0.923, 'unlock_kamikaze', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_fast_troop_red", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=(self.img_archery, w*0.353, h*0.923, 'unlock_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS ET UPGRADER, VOIR LEURS COMMMENTAIRES SI BESOIN
class upgraderBis(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 600, "RTS_UpgraderBis_blue.png", (120, 120), x, y, 130, 130, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 600, "RTS_UpgraderBis_red.png", (120, 120), x, y, 130, 130, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam

        w, h = pygame.display.get_surface().get_size()

        if self.idteam == 1:
            self.img_debuff = pygame.image.load('pictures/sprite/RTS_debuff_blue.png').convert_alpha()
            self.img_debuff = pygame.transform.scale(self.img_debuff, (w * 7 / 160, h * 1 / 13.2))
            self.debuff_img = pygame.image.load('pictures/sprite/RTS_debuff_blue.png').convert_alpha()
            self.debuff_img = pygame.transform.scale(self.debuff_img, (w*7/160, h*1/13.2))

            self.img_siege_troop = pygame.image.load('pictures/sprite/RTS_siege_troop_blue.png').convert_alpha()
            self.img_siege_troop = pygame.transform.scale(self.img_siege_troop, (w*7/160, h*1/13.2))
            self.siege_troop_img = pygame.image.load('pictures/sprite/RTS_siege_troop_blue.png').convert_alpha()
            self.siege_troop_img = pygame.transform.scale(self.siege_troop_img, (w*7/160, h*1/13.2))

        else:
            self.img_debuff = pygame.image.load('pictures/sprite/RTS_debuff_red.png').convert_alpha()
            self.img_debuff = pygame.transform.scale(self.img_debuff, (w * 7 / 160, h * 1 / 13.2))
            self.debuff_img = pygame.image.load('pictures/sprite/RTS_debuff_red.png').convert_alpha()
            self.debuff_img = pygame.transform.scale(self.debuff_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_siege_troop = pygame.image.load('pictures/sprite/RTS_siege_troop_red.png').convert_alpha()
            self.img_siege_troop = pygame.transform.scale(self.img_siege_troop, (w*7/160, h*1/13.2))
            self.siege_troop_img = pygame.image.load('pictures/sprite/RTS_siege_troop_red.png').convert_alpha()
            self.siege_troop_img = pygame.transform.scale(self.siege_troop_img, (w*7/160, h*1/13.2))

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()

        if not self.gamemaster.fast_troop_archery_unlocked:
            self.locked_color(self.img_debuff)
        else:
            self.img_debuff = self.debuff_img

        if not self.gamemaster.siege_troop_unlocked:
            self.locked_color(self.img_siege_troop)
        else:
            self.img_siege_troop = self.siege_troop_img

        if self.idteam == 1:
            if self.action_ui:
                if self.action_ui[0]=="unlocking":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_UpgraderBis_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_UpgraderBis_blue", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_tank_blue", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_tank', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=(self.img_siege_troop, w*0.217, h*0.923, 'unlock_siege_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_fast_troop_archery_blue", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=(self.img_debuff, w*0.353, h*0.923, 'unlock_debuff', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_ui=("RTS_unlocking_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id)) 
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_UpgraderBis_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_UpgraderBis_blue", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_tank_blue", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_tank', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=(self.img_siege_troop, w*0.217, h*0.923, 'unlock_siege_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_fast_troop_archery_blue", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=(self.img_debuff, w*0.353, h*0.923, 'unlock_debuff', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),)
        else:
            if self.action_ui:
                if self.action_ui[0]=="unlocking":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_UpgraderBis_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_UpgraderBis_red", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_tank_red", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_tank', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=(self.img_siege_troop, w*0.217, h*0.923, 'unlock_siege_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_fast_troop_archery_red", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=(self.img_debuff, w*0.353, h*0.923, 'unlock_debuff', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_ui=("RTS_unlocking_ui", (w, int(w*502/1535)), 0-w*1/200, h*0.425, 0, self, 0, 0, id))
    
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_UpgraderBis_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_UpgraderBis_red", (w*1/16, h*1/9), w*0.925, h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_unlock_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_tank_red", (w*7/160, h*1/13.2), w*0.149, h*0.923, 'unlock_tank', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=(self.img_siege_troop, w*0.217, h*0.923, 'unlock_siege_troop', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_fast_troop_archery_red", (w*7/160, h*1/13.2), w*0.285, h*0.923, 'unlock_fast_troop_archery', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=(self.img_debuff, w*0.353, h*0.923, 'unlock_debuff', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS ET UPGRADER, VOIR LEURS COMMMENTAIRES SI BESOIN
class Producer(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 300, "RTS_Producer_blue.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 300, "RTS_Producer_red.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam

        w, h = pygame.display.get_surface().get_size()

        if self.idteam == 1:
            self.img_lowcost = pygame.image.load('pictures/sprite/RTS_lowcost_blue.png').convert_alpha()
            self.img_lowcost = pygame.transform.scale(self.img_lowcost, (w * 7 / 160, h * 1 / 13.2))
            self.lowcost_img = pygame.image.load('pictures/sprite/RTS_lowcost_blue.png').convert_alpha()
            self.lowcost_img = pygame.transform.scale(self.lowcost_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_kamikaze = pygame.image.load('pictures/sprite/RTS_kamikaze_blue.png').convert_alpha()
            self.img_kamikaze = pygame.transform.scale(self.img_kamikaze, (w * 7 / 160, h * 1 / 13.2))
            self.kamikaze_img = pygame.image.load('pictures/sprite/RTS_kamikaze_blue.png').convert_alpha()
            self.kamikaze_img = pygame.transform.scale(self.kamikaze_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_fast_troop = pygame.image.load('pictures/sprite/RTS_fast_troop_blue.png').convert_alpha()
            self.img_fast_troop = pygame.transform.scale(self.img_fast_troop, (w * 7 / 160, h * 1 / 13.2))
            self.fast_troop_img = pygame.image.load('pictures/sprite/RTS_fast_troop_blue.png').convert_alpha()
            self.fast_troop_img = pygame.transform.scale(self.fast_troop_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_archery = pygame.image.load('pictures/sprite/RTS_archery_blue.png').convert_alpha()
            self.img_archery = pygame.transform.scale(self.img_archery, (w * 7 / 160, h * 1 / 13.2))
            self.archery_img = pygame.image.load('pictures/sprite/RTS_archery_blue.png').convert_alpha()
            self.archery_img = pygame.transform.scale(self.archery_img, (w * 7 / 160, h * 1 / 13.2))
        else:
            self.img_lowcost = pygame.image.load('pictures/sprite/RTS_lowcost_red.png').convert_alpha()
            self.img_lowcost = pygame.transform.scale(self.img_lowcost, (w * 7 / 160, h * 1 / 13.2))
            self.lowcost_img = pygame.image.load('pictures/sprite/RTS_lowcost_red.png').convert_alpha()
            self.lowcost_img = pygame.transform.scale(self.lowcost_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_kamikaze = pygame.image.load('pictures/sprite/RTS_kamikaze_red.png').convert_alpha()
            self.img_kamikaze = pygame.transform.scale(self.img_kamikaze, (w * 7 / 160, h * 1 / 13.2))
            self.kamikaze_img = pygame.image.load('pictures/sprite/RTS_kamikaze_red.png').convert_alpha()
            self.kamikaze_img = pygame.transform.scale(self.kamikaze_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_fast_troop = pygame.image.load('pictures/sprite/RTS_fast_troop_red.png').convert_alpha()
            self.img_fast_troop = pygame.transform.scale(self.img_fast_troop, (w * 7 / 160, h * 1 / 13.2))
            self.fast_troop_img = pygame.image.load('pictures/sprite/RTS_fast_troop_red.png').convert_alpha()
            self.fast_troop_img = pygame.transform.scale(self.fast_troop_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_archery = pygame.image.load('pictures/sprite/RTS_archery_red.png').convert_alpha()
            self.img_archery = pygame.transform.scale(self.img_archery, (w * 7 / 160, h * 1 / 13.2))
            self.archery_img = pygame.image.load('pictures/sprite/RTS_archery_red.png').convert_alpha()
            self.archery_img = pygame.transform.scale(self.archery_img, (w * 7 / 160, h * 1 / 13.2))

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()

        if not self.gamemaster.low_cost_unlocked:
            self.locked_color(self.img_lowcost)
        else:
            self.img_lowcost = self.lowcost_img

        if not self.gamemaster.kamikaze_unlocked:
            self.locked_color(self.img_kamikaze)
        else:
            self.img_kamikaze = self.kamikaze_img

        if not self.gamemaster.fast_troop_unlocked:
            self.locked_color(self.img_fast_troop)
        else:
            self.img_fast_troop = self.fast_troop_img

        if not self.gamemaster.archery_unlocked:
            self.locked_color(self.img_archery)
        else:
            self.img_archery = self.archery_img

        if self.idteam == 1:
            if self.action_ui:
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                    "RTS_Producer_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
                    "RTS_Producer_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                    btn1=(
                    self.img_lowcost, w*0.149, h*0.923, 'create_lowcost', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn2=(
                    self.img_kamikaze, w*0.217, h*0.923, 'create_kamikaze', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn3=(
                    self.img_fast_troop, w*0.285, h*0.923, 'create_fast_troop', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn4=(
                    self.img_archery, w*0.353, h*0.923, 'create_archery', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn6=(
                    "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                    self.pos_absolu_y), 
                    img_action=(
                    "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, 0, self.gamemaster,
                    0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, 
                                                 image1=("RTS_Producer_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                                                 image2=("RTS_Producer_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                                                 image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                                                 btn1=(self.img_lowcost, w*0.149, h*0.923, 'create_lowcost', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn2=(self.img_kamikaze, w*0.217, h*0.923, 'create_kamikaze', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn3=(self.img_fast_troop, w*0.285, h*0.923, 'create_fast_troop', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn4=(self.img_archery, w*0.353, h*0.923, 'create_archery', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn6=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,self.pos_absolu_y))
        else:
            if self.action_ui:
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, 
                    image1=(
                    "RTS_Producer_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image2=(
                    "RTS_Producer_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                    btn1=(
                    self.img_lowcost, w*0.149, h*0.923, 'create_lowcost', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn2=(
                    self.img_kamikaze, w*0.217, h*0.923, 'create_kamikaze', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn3=(
                    self.img_fast_troop, w*0.285, h*0.923, 'create_fast_troop', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn4=(
                    self.img_archery, w*0.353, h*0.923, 'create_archery', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn6=(
                    "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                    self.pos_absolu_y), 
                    img_action=(
                    "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, 0, self.gamemaster,
                    0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster,
                                                 image1=(
                "RTS_Producer_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, 0, 0, 0, 0, 0), 
                image2=(
                "RTS_Producer_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, 0, 0, 0, 0, 0), 
                image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                btn1=(
                self.img_lowcost, w*0.149, h*0.923, 'create_lowcost', self.gamemaster,
                self.pos_absolu_x, self.pos_absolu_y),
                btn2=(
                    self.img_kamikaze, w*0.217, h*0.923, 'create_kamikaze', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                btn3=(
                self.img_fast_troop, w*0.285, h*0.923, 'create_fast_troop', self.gamemaster,
                self.pos_absolu_x, self.pos_absolu_y),
                btn4=(
                    self.img_archery, w*0.353, h*0.923, 'create_archery', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                btn6=(
                "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                self.pos_absolu_y))
        
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS ET UPGRADER, VOIR LEURS COMMMENTAIRES SI BESOIN
class ProducerBis(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 300, "RTS_ProducerBis_blue.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 300, "RTS_ProducerBis_red.png", (125, 125), x, y, 140, 140, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam

        w, h = pygame.display.get_surface().get_size()

        if self.idteam == 1:
            self.img_tank = pygame.image.load('pictures/sprite/RTS_tank_blue.png').convert_alpha()
            self.img_tank = pygame.transform.scale(self.img_tank, (w * 7 / 160, h * 1 / 13.2))
            self.tank_img = pygame.image.load('pictures/sprite/RTS_tank_blue.png').convert_alpha()
            self.tank_img = pygame.transform.scale(self.tank_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_fast_troop_archery = pygame.image.load('pictures/sprite/RTS_fast_troop_archery_blue.png').convert_alpha()
            self.img_fast_troop_archery = pygame.transform.scale(self.img_fast_troop_archery, (w*7/160, h*1/13.2))
            self.fast_troop_archery_img = pygame.image.load('pictures/sprite/RTS_fast_troop_archery_blue.png').convert_alpha()
            self.fast_troop_archery_img = pygame.transform.scale(self.fast_troop_archery_img, (w*7/160, h*1/13.2))

            self.img_debuff = pygame.image.load('pictures/sprite/RTS_debuff_blue.png').convert_alpha()
            self.img_debuff = pygame.transform.scale(self.img_debuff, (w * 7 / 160, h * 1 / 13.2))
            self.debuff_img = pygame.image.load('pictures/sprite/RTS_debuff_blue.png').convert_alpha()
            self.debuff_img = pygame.transform.scale(self.debuff_img, (w*7/160, h*1/13.2))

            self.img_siege_troop = pygame.image.load('pictures/sprite/RTS_siege_troop_blue.png').convert_alpha()
            self.img_siege_troop = pygame.transform.scale(self.img_siege_troop, (w*7/160, h*1/13.2))
            self.siege_troop_img = pygame.image.load('pictures/sprite/RTS_siege_troop_blue.png').convert_alpha()
            self.siege_troop_img = pygame.transform.scale(self.siege_troop_img, (w*7/160, h*1/13.2))
        else:
            self.img_tank = pygame.image.load('pictures/sprite/RTS_tank_red.png').convert_alpha()
            self.img_tank = pygame.transform.scale(self.img_tank, (w * 7 / 160, h * 1 / 13.2))
            self.tank_img = pygame.image.load('pictures/sprite/RTS_tank_red.png').convert_alpha()
            self.tank_img = pygame.transform.scale(self.tank_img, (w * 7 / 160, h * 1 / 13.2))

            self.img_fast_troop_archery = pygame.image.load('pictures/sprite/RTS_fast_troop_archery_red.png').convert_alpha()
            self.img_fast_troop_archery = pygame.transform.scale(self.img_fast_troop_archery, (w*7/160, h*1/13.2))
            self.fast_troop_archery_img = pygame.image.load('pictures/sprite/RTS_fast_troop_archery_red.png').convert_alpha()
            self.fast_troop_archery_img = pygame.transform.scale(self.fast_troop_archery_img, (w*7/160, h*1/13.2))

            self.img_debuff = pygame.image.load('pictures/sprite/RTS_debuff_red.png').convert_alpha()
            self.img_debuff = pygame.transform.scale(self.img_debuff, (w * 7 / 160, h * 1 / 13.2))
            self.debuff_img = pygame.image.load('pictures/sprite/RTS_debuff_red.png').convert_alpha()
            self.debuff_img = pygame.transform.scale(self.debuff_img, (w*7/160, h*1/13.2))

            self.img_siege_troop = pygame.image.load('pictures/sprite/RTS_siege_troop_red.png').convert_alpha()
            self.img_siege_troop = pygame.transform.scale(self.img_siege_troop, (w*7/160, h*1/13.2))
            self.siege_troop_img = pygame.image.load('pictures/sprite/RTS_siege_troop_red.png').convert_alpha()
            self.siege_troop_img = pygame.transform.scale(self.siege_troop_img, (w*7/160, h*1/13.2))
           
    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()

        if not self.gamemaster.siege_troop_unlocked:
            self.locked_color(self.img_siege_troop)
        else:
            self.img_siege_troop = self.siege_troop_img

        if not self.gamemaster.tank_unlocked:
            self.locked_color(self.img_tank)
        else:
            self.img_tank = self.tank_img

        if not self.gamemaster.debuff_unlocked:
            self.locked_color(self.img_debuff)
        else:
            self.img_debuff = self.debuff_img

        if not self.gamemaster.fast_troop_archery_unlocked:
            self.locked_color(self.img_fast_troop_archery)
        else:
            self.img_fast_troop_archery = self.fast_troop_archery_img


        if self.idteam == 1:
            if self.action_ui:
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
                    "RTS_ProducerBis_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
                    "RTS_ProducerBis_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                    btn1=(
                    self.img_tank, w*0.149, h*0.923, 'create_tank', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn2=(
                    self.img_siege_troop, w*0.217, h*0.923, 'create_siege_troop', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn3=(
                    self.img_fast_troop_archery, w*0.285, h*0.923, 'create_fast_troop_archery', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y),
                    btn4=(
                    self.img_debuff, w*0.353, h*0.923, 'create_debuff', self.gamemaster,
                    self.pos_absolu_x, self.pos_absolu_y), 
                    btn6=(
                    "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                    self.pos_absolu_y), 
                    img_action=(
                    "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, 0, self.gamemaster,
                    0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, 
                                                 image1=("RTS_ProducerBis_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                                                 image2=("RTS_ProducerBis_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                                                 image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                                                 btn1=(self.img_tank, w*0.149, h*0.923, 'create_tank', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn2=(self.img_siege_troop, w*0.217, h*0.923, 'create_siege_troop', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y), 
                                                 btn3=(self.img_fast_troop_archery, w*0.285, h*0.923, 'create_fast_troop_archery', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                                                 btn4=(self.img_debuff, w*0.353, h*0.923, 'create_debuff', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y), 
                                                 btn6=("delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,self.pos_absolu_y))
        else:
            if self.action_ui:
                if self.action_ui[0] == "creating_troop":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, 
                    image1=(
                    "RTS_ProducerBis_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image2=(
                    "RTS_ProducerBis_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), 
                    image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                    btn1=(self.img_tank, w*0.149, h*0.923, 'create_tank', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                    btn2=(self.img_siege_troop, w*0.217, h*0.923, 'create_siege_troop', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y), 
                    btn3=(self.img_fast_troop_archery, w*0.285, h*0.923, 'create_fast_troop_archery', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                    btn4=(self.img_debuff, w*0.353, h*0.923, 'create_debuff', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y), 
                    btn6=(
                    "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                    self.pos_absolu_y), 
                    img_action=(
                    "action_ui_creating_troop", (w, int(w * 502 / 1535)), 0-w*1/200, h * 0.425, 0, self.gamemaster,
                    0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster,
                                                 image1=(
                "RTS_ProducerBis_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, 0, 0, 0, 0, 0), 
                image2=(
                "RTS_ProducerBis_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, 0, 0, 0, 0, 0), 
                image3=("RTS_create_units_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), 
                btn1=(self.img_tank, w*0.149, h*0.923, 'create_tank', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                btn2=(self.img_siege_troop, w*0.217, h*0.923, 'create_siege_troop', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y), 
                btn3=(self.img_fast_troop_archery, w*0.285, h*0.923, 'create_fast_troop_archery', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),
                btn4=(self.img_debuff, w*0.353, h*0.923, 'create_debuff', self.gamemaster,self.pos_absolu_x, self.pos_absolu_y),     
                btn6=(
                "delete_button", (w*38/160, h*1/21), w*0.6102, h*0.935, 'delete_building', self.gamemaster, self.pos_absolu_x,
                self.pos_absolu_y))
        
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS, VOIR SON COMMMENTAIRE SI BESOIN
class healer(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 340, "RTS_healer_blue.png", (80, 110), x, y, 80, 110, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 340, "RTS_healer_red.png", (80, 110), x, y, 80, 110, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam
        self.idteam = idteam
        self.dgt = 100
        self.att_speed = 1
        self.reach = 50000000
        self.cooldown_attack = 1 / self.att_speed
        self.target_enemy = None


    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        if self.idteam == 1:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_healer_blue", (w * 1 / 18, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_healer_blue", (w * 1 / 18, h * 1 / 9), w * 0.935, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        else:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_healer_red", (w * 1 / 18, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_healer_red", (w * 1 / 18, h * 1 / 9), w * 0.935, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

    def attack(self, id):
        self.target_enemy = id

    def attack_(self):
        try:
            n = self.gamemaster.sprite_list[self.target_enemy]
            if self.target_enemy and (
                    (n.pos_absolu_x - self.pos_absolu_x) ** 2 + (n.pos_absolu_y - self.pos_absolu_y) ** 2) ** (
                    1 / 2) <= self.reach * 10:
                if self.cooldown_attack <= 0:

                        n.HP = max(n.HP + self.dgt, n.HPmax)
                        self.cooldown_attack = 1 / self.att_speed
            if self.gamemaster.clock.get_fps():
                self.cooldown_attack -= 1 / self.gamemaster.clock.get_fps()
        except KeyError:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS, VOIR SON COMMMENTAIRE SI BESOIN
class cannon(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 340, "RTS_cannon_blue.png", (60, 60), x, y, 60, 60, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 340, "RTS_cannon_red.png", (60, 60), x, y, 60, 60, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam
        self.dgt = 100
        self.att_speed = 0.2
        self.reach = 200
        self.cooldown_attack = 1 / self.att_speed
        self.target_enemy = None


    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        if self.idteam == 1:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_cannon_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_cannon_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        else:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_cannon_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_cannon_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

    def attack(self, id):
        self.target_enemy = id

    def attack_(self):
        try:
            n = self.gamemaster.sprite_list[self.target_enemy]
            if self.target_enemy and (
                    (n.pos_absolu_x - self.pos_absolu_x) ** 2 + (n.pos_absolu_y - self.pos_absolu_y) ** 2) ** (
                    1 / 2) <= self.reach * 10:
                if self.cooldown_attack <= 0:
                    n.HP -= self.dgt * (100 - n.ac)
                    self.cooldown_attack = 1 / self.att_speed
                    if n.HP <= 0:
                        n.death()
            if self.gamemaster.clock.get_fps():
                self.cooldown_attack -= 1 / self.gamemaster.clock.get_fps()
        except KeyError:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE NEXUS, VOIR SON COMMMENTAIRE SI BESOIN
class mur(Building):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam == 1:
            Building.__init__(self, 200, "RTS_wall_blue.png", (130, 130), x, y, 120, 120, _gamemaster, id,
                              _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Building.__init__(self, 200, "RTS_wall_red.png", (130, 130), x, y, 120, 120, _gamemaster, id,
                              _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster = _gamemaster
        self.x_dep = x - 25
        self.y_dep = y + 50
        self.idteam = idteam
        self.dgt = 100
        self.att_speed = 1
        self.reach = 50000000
        self.cooldown_attack = 1 / self.att_speed
        self.target_enemy = None


    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        if self.idteam == 1:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_wall_blue", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_wall_blue", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        else:
            self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=(
            "RTS_wall_red", (w * 1 / 16, h * 1 / 9), w * 0.01, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), image2=(
            "RTS_wall_red", (w * 1 / 16, h * 1 / 9), w * 0.925, h * 0.88, 0, self.gamemaster, 0, 0, 0, 0), btn_del=(
            "delete_button", (w * 38 / 160, h * 1 / 21), w * 0.6102, h * 0.935, 'delete_building', self.gamemaster,
            self.pos_absolu_x, self.pos_absolu_y))
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass



class Minerai(pygame.sprite.Sprite): # Minerai qui est aussi une classe sprite
    def __init__(self, quantity, img, size, _gamemaster, coord_x, coord_y, width, height, id):
        pygame.sprite.Sprite.__init__(self)
        
        # Initialisation des attributs avec les arguments
        self.gamemaster = _gamemaster
        self.quantity = quantity
        self.width = width
        self.height = height
        self.id = id
        self.idteam = -1

        self.img = pygame.image.load(f'pictures/sprite/{img}').convert_alpha()
        self.image = pygame.transform.scale(self.img, size)
        self.rect = self.image.get_rect()

        self.rect.x = coord_x
        self.rect.y = coord_y
        self.pos_absolu_x = coord_x
        self.pos_absolu_y = coord_y

        self.offscreen = False

# Fonction qui doivent exister car c'est aussi un sprite et a donc des fonctions communes avec les autres sprites, on veut pas de messages d'erreur
    def attack_(self):
        return False

    def mine(self, id):
        return False

    def mine_(self):
        return False

    def move(self):
        return False

    def _move(self):
        return False

    def click(self, pos, n=True):
        return False

    def death(self):
        self.gamemaster.suppr_sprite(self.id, self.allyteam)

    def update_pos(self, offset_x, offset_y):
        # on met à jour la position du rectangle en fonction de la caméra
        self.rect.x = self.pos_absolu_x - offset_x
        self.rect.y = self.pos_absolu_y - offset_y
        # on voit si le sprite est en dehors de l'écran
        if self.rect.x < 0 or self.rect.x > pygame.display.get_surface().get_size()[
            0] or self.rect.y < 0 or self.rect.y > pygame.display.get_surface().get_size()[0]:
            return False
        return True

# Initialisation des deux minerais
class Iron(Minerai):
    def __init__(self, quantity, coord_x, coord_y, _gamemaster, id):
        Minerai.__init__(self, quantity, "minerai_de_fer.png", (50, 50), _gamemaster, coord_x, coord_y, 50, 50, id)
        self.type = 1

class Gold(Minerai):
    def __init__(self, quantity, coord_x, coord_y, _gamemaster, id):
        Minerai.__init__(self, quantity, "minerai_d_or.png", (50, 50), _gamemaster, coord_x, coord_y, 50, 50, id)
        self.type = 2
