import pygame
import graphic
import update
import gamemaster

clock = None

def connectserv(ip): # Connection au serveur
    global clock
    clock = pygame.time.Clock()
    gameMaster = gamemaster.GameMaster(clock,ip)
    return gameMaster

def run(gameMaster): # Fait tourner le jeu
    GameOn = True

    gameMaster.start()
    FPS = 30
    while True:
        if gameMaster.inrun == True:
            g = graphic.Graphics()
            gameMaster.run()
            iron = gameMaster.iron
            gold = gameMaster.gold
            supply = gameMaster.supply
            supply_max = gameMaster.supply_max
            w, h = pygame.display.get_surface().get_size()

            # Tant que le jeu tourne, il tourne  :)
            while True:
                gameMaster.update() # Met à jour le joueur
                
                # Affiche le nombre de ressources
                g.screen.blit(gameMaster.iron_text, [w*0.05, h*0.053])
                g.screen.blit(gameMaster.gold_text, [w*0.092, h*0.053])
                g.screen.blit(gameMaster.supply_text, [w*0.075, h*0.108])

                # Change l'écriture à afficher qi le nombre de ressources changent
                if gameMaster.iron != iron:
                    iron = gameMaster.iron
                    gameMaster.iron_text = pygame.font.SysFont("arial", int(w*0.014)).render(str(int(gameMaster.iron)), False, (255, 255, 255))
                if gameMaster.gold != gold:
                    gold = gameMaster.gold
                    gameMaster.gold_text = pygame.font.SysFont("arial", int(w*0.014)).render(str(int(gameMaster.gold)), False, (255, 255, 255))
                if gameMaster.supply != supply:
                    supply = gameMaster.supply
                    gameMaster.supply_text = pygame.font.SysFont("arial", int(w*0.009)).render(str(int(gameMaster.supply))+" / "+str(int(gameMaster.supply_max)), False, (255, 255, 255))
                if gameMaster.supply_max != supply_max:
                    supply_max = gameMaster.supply_max
                    gameMaster.supply_text = pygame.font.SysFont("arial", int(w*0.009)).render(str(int(gameMaster.supply))+" / "+str(int(gameMaster.supply_max)), False, (255, 255, 255))

                if gameMaster.actived_obj: 
                    if gameMaster.actived_obj[0].action: # Affiche l'image de l'objet (bâtiment) qui est amené à être construit
                        gameMaster.blit_pre_img(g)

                if gameMaster.end_of_game_bol: # En fin de jeu; l'affiche
                    gameMaster.end_of_game__(gameMaster.loser)

                # Met à jour l'écran
                pygame.display.flip()
                clock.tick(FPS)

        clock.tick(FPS)
