import copy

import pygame
import UI
import algo
import buildings
import numpy as np

class Troops(pygame.sprite.Sprite):
    def __init__(self, HP, dmg, att_speed, mvmt_speed, reach, ac, sight, supply_size, img, rect, size, x, y, width, height, _gamemaster, id, allyteam):
        pygame.sprite.Sprite.__init__(self)

        # Initilisation des variables communes à toutes les unités
        self.on_click_ = False
        self.HP = HP
        self.dmg = dmg
        self.att_speed = att_speed
        self.mvmt_speed = mvmt_speed
        self.reach = reach
        self.ac = ac
        self.sight = sight
        self.supply_size = supply_size
        self.width = width
        self.height = height
        self.gamemaster = _gamemaster
        self.target_enemy = False
        self.action=[]
        self.action_ui = []
        self.cd = 0
        self.allyteam = allyteam
        self.n = 0
        self.maxHP= HP
        self.mine_targer = None

        if self.allyteam:
            self.gamemaster.supply += self.supply_size

        self.cooldown_attack = 1/att_speed

        self.id = id



        self.rect = None


        # Vérifie si l'objet à une image et adapte son rectangle en conséquence
        if rect != None:
            self.image = img
            self.rect = rect
        else:
            self.img = pygame.image.load(f'pictures/sprite/{img}.png').convert_alpha()
            self.image = pygame.transform.scale(self.img, size)
            self.rect = self.image.get_rect(topleft=(x, y))

        # Met à jour la position de l'unité
        self.pos_absolu_x = self.rect.x
        self.pos_absolu_y = self.rect.y

        self._path = []

        self.target_x = self.pos_absolu_x
        self.target_y = self.pos_absolu_y
        self.update_pos(self.gamemaster.offset_x, self.gamemaster.offset_y)
        self.offscreen = False
        
        if self.allyteam: # Si l'unité est une unité allié au joueur, le brouillard diminue
            for i in range(int(self.pos_absolu_x/10)-self.sight,int(self.pos_absolu_x/10)+self.sight+1):
                for j in range(int(self.pos_absolu_y / 10) - 10, int(self.pos_absolu_y / 10) + 11):
                    self.gamemaster.brouillard_grille[i][j] = 1

    def check_onclick(self): # Vérifie la collision avec la sourie
        mouse_pos = pygame.mouse.get_pos()
        if self.rect is not None:
            if self.rect.collidepoint(mouse_pos):
                return self.rect.collidepoint(mouse_pos)

    def update(self): # Effectue la fonction de l'unité
        if self.check_onclick():
            self.click()

    def attack(self, id): # Cible l'nuité adverse à attaquer
        self.target_enemy = id

    def attack_(self):
        try:
            n = self.gamemaster.sprite_list[self.target_enemy] # Selection l'unité à attaquer
            if self.target_enemy and ((n.pos_absolu_x-self.pos_absolu_x)**2+(n.pos_absolu_y-self.pos_absolu_y)**2)**(1/2) <= self.reach*20 : # Si l'unité est à porté de l'attaquant
                if self.cooldown_attack <= 0: # Attaque en fonction de la vitesse d'attaque de l'unité
                    n.HP -= self.dmg*(100-n.ac) # Attribution des dégats
                    self.cooldown_attack = 1/self.att_speed
                    if n.HP <= 0: # Tue le sprite adverse quand pv<=0
                        n.death()
            if self.gamemaster.clock.get_fps(): # met à jour le cooldown de l'unité
                self.cooldown_attack -= 1/self.gamemaster.clock.get_fps()
        except KeyError:
            pass

    def death(self): # Mort, suppression du sprite et diminution du nombre maximal d'unité disponible
        if self.allyteam:
            self.gamemaster.supply-=self.supply_size
        self.delete_ui()
        self.gamemaster.suppr_sprite(self.id)

    def move(self, _grid, target_x, target_y):
        # récupère la position de départ dans la grille
        start = (int(self.pos_absolu_x / 10), int(self.pos_absolu_y / 10))

        # récupère la position absolue d'arrivée
        self.target_x = target_x
        self.target_y = target_y

        # récupère la position d'arrivée dans la grille
        end = (int(target_x / 10), int(target_y / 10))

        if self.gamemaster.fence.grille[end[0]][end[1]]==1:
            # cherche le chemin le plus rapide
            path = algo.search_path(_grid, start, end)
            if path == None:
                path = [0]
            del path[0]

            # enregistre le chemin pour ne pas à avoir à le recalculer
            self._path = path

            # fait bouger le sprite d'autant de fois que sa vitesse
            for i in range(self.mvmt_speed):
                try:
                    del self._path[0]
                    self.pos_absolu_x = self._path[0].x * 10
                    self.pos_absolu_y = self._path[0].y * 10

                except IndexError:
                    pass


    def _move(self):
        # fait bouger le sprite d'autant de fois que sa vitesse
        try:
            for i in range(int(self.mvmt_speed)):
                    if self.n%8 == 0:
                        del self._path[0]
                    if len(self._path) > 1:
                        self.pos_absolu_x *= 2
                        self.pos_absolu_x += self._path[0].x * 10
                        self.pos_absolu_x /= 3
                        self.pos_absolu_y *= 2
                        self.pos_absolu_y += self._path[0].y * 10
                        self.pos_absolu_y /= 3
                        self.n += 1

        except:
            pass
        if self.allyteam:
            for i in range(int(self.pos_absolu_x/10)-self.sight,int(self.pos_absolu_x/10)+self.sight+1):
                for j in range(int(self.pos_absolu_y / 10) - 10, int(self.pos_absolu_y / 10) + 11):
                    try:
                        self.gamemaster.brouillard_grille[i][j] = 1
                    except:
                        pass
                    
    def update_pos(self, offset_x, offset_y):
        # on met à jour la position du rectangle en fonction de la caméra
        self.rect.x = self.pos_absolu_x - offset_x
        self.rect.y = self.pos_absolu_y - offset_y
        # on voit si le sprite est en dehors de l'écran
        if self.rect.x < 0 or self.rect.x > pygame.display.get_surface().get_size()[
            0] or self.rect.y < 0 or self.rect.y > pygame.display.get_surface().get_size()[0] or self.gamemaster.brouillard_grille[int(self.pos_absolu_x / 10)][int(self.pos_absolu_y / 10)] == 0:
            self.offscreen = False
            return self.offscreen
        self.offscreen = True
        return self.offscreen

    def click(self, pos,n = True):
        # Vérifie que l'unité soit cliqué, si oui effectue la fonction suivante
        if self.rect.collidepoint(pos):
            if self.gamemaster.actived_obj and n:
                if self.gamemaster.actived_obj[0].action:
                    del self.gamemaster.actived_obj[0].action[0]

            self.on_click()
            return True
        return False

    def on_click(self):
        self.on_click_ = True
        # si un élément est actif et est différent à celui qui vient d'être activé, il est remplacé
        # si aucun élément n'est activé, celui-ci s'active
        if len(self.gamemaster.actived_obj)>=1:
            if self.gamemaster.actived_obj[0]!=self:
                self.gamemaster.actived_obj[0].delete_ui()
                del self.gamemaster.actived_obj[0]
                self.generate_ui()
        else:
            self.generate_ui()

    def target_nexus(self,coord_x,coord_y):
        return False

    def order_management(self, order, grid, x, y):
        if int(order) == 1:
            self.move(np.ndarray(grid), x, y)

    def mine(self,id):
        return False
    
    def mine_(self):
        return False
    
    def delete_ui(self):# Suppression de l'affichege de l'unité
        try:
            self.building_opt.delete_ui()
        except:
            pass

class worker(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam,fusion_tier = 1):
        if idteam==1:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_Worker_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_Worker_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        # Initialisation des attrbuts de l'objet en fonction des arguments
        self.gamemaster=_gamemaster
        self.mine_targer = None
        self.nexus = None
        self.depo_pos = (-1,-1)
        self.etape = 0
        self.carry = 0
        self.first = False
        self.max_carry = 10*fusion_tier
        self.idteam = idteam
        self.max_carry_gold = 1*fusion_tier
        self.vitesse_mine = 2*fusion_tier
        self.vitesse_mine_gold= 0.2*fusion_tier
        self.fusion_tier = fusion_tier
        self.type = None
        self.fusiontargetid = None
        self.gamemaster.worker+=1

# TOUTES LES FONCTIONS GENERATE_UI SONT CONSTRUITES DE LA MEME MANIERE, LE COMMENTAIRE DE CE CODE S'APPLIQUE DONC AUX AUTRES
    def generate_ui(self): # Affichage du menu de l'unité
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj.append(self)
        
        try: # Essaie de supprimer l'élément actif du joueur
            del self.gamemaster.actived_obj[0]
        except:
            pass
        
        self.gamemaster.actived_obj.append(self) # Ajoute cet élément en tant qu'élément actif du joueur

        if self.idteam == 1: # Affiche le menu de l'unité selon son équipe
            if self.action_ui: # Si une action en cours qui doit être affichée existe, change l'affichage du building                  
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Worker_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Worker_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_Upgrader_blue", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_upgrader', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_Nexus_blue", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_nexus', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_Producer_blue", (w*7/160, h*1/13.2), w*0.336, h*0.923, 'create_Producer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn4=("RTS_supply_blue", (w*7/160, h*1/13.2), w*0.2,  h*0.923, 'create_supply', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Worker_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Worker_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_Upgrader_blue", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_upgrader', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_Nexus_blue", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_nexus', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_Producer_blue", (w*7/160, h*1/13.2), w*0.336, h*0.923, 'create_Producer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn4=("RTS_supply_blue", (w*7/160, h*1/13.2), w*0.2,  h*0.923, 'create_supply', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.4560,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:     
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Worker_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Worker_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_Upgrader_red", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_upgrader', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_Nexus_red", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_nexus', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_Producer_red", (w*7/160, h*1/13.2), w*0.336, h*0.923, 'create_Producer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn4=("RTS_supply_red", (w*7/160, h*1/13.2), w*0.2,  h*0.923, 'create_supply', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.4560,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_Worker_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_Worker_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_Upgrader_red", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_upgrader', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn2=("RTS_Nexus_red", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_nexus', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_Producer_red", (w*7/160, h*1/13.2), w*0.336, h*0.923, 'create_Producer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn4=("RTS_supply_red", (w*7/160, h*1/13.2), w*0.2,  h*0.923, 'create_supply', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.456,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))

    def delete_ui(self): # Essaie la suppression de l'affichage de l'objet
        try:
            self.building_opt.delete_ui()
        except:
            pass
        
    def death(self): # Mort et suppression du sprite en mettant à jour le nombre possible d'unités en jeu
        self.gamemaster.supply-=self.supply_size
        self.gamemaster.worker-=1
        self.delete_ui()
        self.gamemaster.suppr_sprite(self.id)
        
    def fusion(self,id): # Fusion entre ce sprite et celui du sprite ciblé avec l'id
        self.fusiontargetid = id
        self.gamemaster.move(self.id,self.gamemaster.sprite_list[id].pos_absolu_x,self.gamemaster.sprite_list[id].pos_absolu_y)


    def mine(self,id):
        self.mine_targer = id # Choisi la minerai sur lequel mine ce sprite

        if self.depo_pos != (-1,-1) : # Initialise le chemin à emprunter pour atteindre le minerai et le nexus
            self.first = True
            self.etape = 0
            self.to_mine = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) ,(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)))
            self.to_base = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) )
            print(self.to_mine,'ee',self.to_base)
            del self.to_mine[0]
            del self.to_base[0]
            self.type= self.gamemaster.sprite_list[self.mine_targer].type

    def target_nexus(self,coord_x,coord_y): # Cible le nexus grâce aux coordonnées
        self.depo_pos = (coord_x,coord_y)
        try:
            self.mine()
        except:
            pass

    def mine_(self):
        if self.fusiontargetid: # Fusionne avec une autre sprite si cela doit être le cas
            if ((self.pos_absolu_x-self.gamemaster.sprite_list[self.fusiontargetid].pos_absolu_x)**2 + (self.pos_absolu_y-self.gamemaster.sprite_list[self.fusiontargetid].pos_absolu_y)**2)**(1/2) <=150:
                self.gamemaster.sprite_list[self.fusiontargetid].death()
                self.death()
                self.gamemaster.create_worker(self.pos_absolu_x,self.pos_absolu_y,self.fusion_tier + self.gamemaster.sprite_list[self.fusiontargetid].fusion_tier)
                self.fusiontargetid = None
        
        
        if self.depo_pos != (-1,-1) and self.mine_targer != None :  # Dépose les ressources au nexus ciblé avant de repartir vers le minerai qu'il exploite
            if self.etape == 0:
                if ((self.pos_absolu_x - self.depo_pos[0])**2 + (self.pos_absolu_y - self.depo_pos[1])**2)**(1/2) <= 25 or self.first:
                    if self.type == 1 and self.allyteam:
                        self.gamemaster.iron += self.carry
                    if self.type == 2 and self.allyteam:
                        self.gamemaster.gold += self.carry
                    self.carry = 0
                    if self.first:
                        self.gamemaster.move( self.id,self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x, self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)
                    else:
                        self._path = copy.copy(self.to_mine)

                    self.etape = 1
                    self.first = False
            
            if self.etape == 1:
                if ((self.pos_absolu_x - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x)**2 + (self.pos_absolu_y - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)**2)**(1/2) <=30:
                    self.etape = 2
            
            if self.etape == 2:
                if self.type == 1:
                    self.carry += self.vitesse_mine/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                elif self.type == 2:
                    self.carry += self.vitesse_mine_gold/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 0.2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry_gold:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                if self.gamemaster.sprite_list[self.mine_targer].quantity <=0:
                    self.gamemaster.sprite_list[self.mine_targer].death()
                    self._path = copy.copy(self.to_base)
                    self.etape = 0

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN
class workerBis(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam, fusion_tier = 1):
        if idteam==1:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_workerBis_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_workerBis_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.mine_targer = None
        self.nexus = None
        self.depo_pos = (-1,-1)
        self.etape = 0
        self.carry = 0
        self.first = False
        self.max_carry = 10*fusion_tier
        self.idteam = idteam
        self.max_carry_gold = 1*fusion_tier
        self.vitesse_mine = 2*fusion_tier
        self.vitesse_mine_gold= 0.2*fusion_tier
        self.fusion_tier = fusion_tier
        self.type = None
        self.fusiontargetid = None
        self.gamemaster.worker+=1

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj.append(self)
        try:
            del self.gamemaster.actived_obj[0]
        except:
            pass
        self.gamemaster.actived_obj.append(self)

        if self.idteam == 1:
            if self.action_ui:                  
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerBis_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerBis_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_UpgraderBis_blue", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_upgraderBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y),btn3=("RTS_ProducerBis_blue", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_ProducerBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerBis_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerBis_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0),  btn1=("RTS_UpgraderBis_blue", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_upgraderBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_ProducerBis_blue", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_ProducerBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:     
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerBis_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerBis_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0),  btn1=("RTS_UpgraderBis_red", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_upgraderBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_ProducerBis_red", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_ProducerBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerBis_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerBis_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn1=("RTS_UpgraderBis_red", (w*7/160, h*1/13.2), w*0.132, h*0.923, 'create_upgraderBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn3=("RTS_ProducerBis_red", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_ProducerBis', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONBIS', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

    def mine(self,id):
        if self.depo_pos != (-1, -1):
            self.mine_targer = id
            self.first = True
            self.etape = 0
            self.to_mine = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) ,(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)))
            self.to_base = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) )
            del self.to_mine[0]
            del self.to_base[0]
            self.type= self.gamemaster.sprite_list[self.mine_targer].type

    def target_nexus(self,coord_x,coord_y):
        self.depo_pos = (coord_x,coord_y)
        try:
            self.mine()
        except:
            pass
    def fusion(self,id):
        self.fusiontargetid = id
        self.gamemaster.move(self.id,self.gamemaster.sprite_list[id].pos_absolu_x,self.gamemaster.sprite_list[id].pos_absolu_y)

    def mine_(self):
        if self.fusiontargetid:
            if ((self.pos_absolu_x-self.gamemaster.sprite_lisat[self.fusiontargetid].pos_absolu_x)**2 + (self.pos_absolu_y-self.gamemaster.sprite_list[self.fusiontargetid].pos_absolu_y)**2)**(1/2) <=150:
                self.gamemaster.sprite_list[self.fusiontargetid].death()
                self.death()
                self.gamemaster.create_workerBis(self.pos_absolu_x,self.pos_absolu_y,self.fusion_tier + self.gamemaster.sprite_list[self.fusiontargetid].fusion_tier)
                self.fusiontargetid = None
        if self.depo_pos != (-1,-1) and self.mine_targer != None :
            if self.etape == 0:
                if ((self.pos_absolu_x - self.depo_pos[0])**2 + (self.pos_absolu_y - self.depo_pos[1])**2)**(1/2) <= 25 or self.first:
                    if self.type == 1 and self.allyteam:
                        self.gamemaster.iron += self.carry
                    if self.type == 2 and self.allyteam:
                        self.gamemaster.gold += self.carry
                    self.carry = 0
                    if self.first:
                        self.gamemaster.move( self.id,self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x, self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)
                    else:
                        self._path = copy.copy(self.to_mine)
                    self.etape = 1
                    self.first = False
            if self.etape == 1:

                if ((self.pos_absolu_x - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x)**2 + (self.pos_absolu_y - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)**2)**(1/2) <=30:
                    self.etape = 2
            if self.etape == 2:
                if self.type == 1:
                    self.carry += 2/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                elif self.type == 2:
                    self.carry += 0.2/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 0.2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry_gold:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                if self.gamemaster.sprite_list[self.mine_targer].quantity <=0:
                    self.gamemaster.sprite_list[self.mine_targer].death()
                    self._path = copy.copy(self.to_base)
                    self.etape = 0

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN
class workerTrd(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam,fusion_tier):
        if idteam==1:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_workerTrd_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 24*fusion_tier, 1*fusion_tier, 1, 4, 3, 0, 20, 1, "RTS_workerTrd_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.mine_targer = None
        self.nexus = None
        self.depo_pos = (-1,-1)
        self.etape = 0
        self.carry = 0
        self.first = False
        self.max_carry = 10*fusion_tier
        self.idteam = idteam
        self.max_carry_gold = 1*fusion_tier
        self.vitesse_mine = 2*fusion_tier
        self.vitesse_mine_gold= 0.2*fusion_tier
        self.fusion_tier = fusion_tier
        self.type = None
        self.fusiontargetid = None
        self.gamemaster.worker+=1

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj.append(self)
        try:
            del self.gamemaster.actived_obj[0]
        except:
            pass
        self.gamemaster.actived_obj.append(self)

        if self.idteam == 1:
            if self.action_ui:                  
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerTrd_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerTrd_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn3=("RTS_cannon_blue", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_cannon', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=("RTS_healer_blue", (w*7/210, h*1/14), w*0.14, h*0.9255, 'create_healer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_wall_blue", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_wall', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerTrd_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerTrd_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn3=("RTS_cannon_blue", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_cannon', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=("RTS_healer_blue", (w*7/210, h*1/14), w*0.14, h*0.9255, 'create_healer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_wall_blue", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_wall', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:     
                if self.action_ui[0] == "building":
                    self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerTrd_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerTrd_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn3=("RTS_cannon_red", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_cannon', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=("RTS_healer_red", (w*7/210, h*1/14), w*0.14, h*0.9255, 'create_healer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_wall_red", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_wall', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), img_action = ("action_ui_building", (w, int(w*502/1535)), 0, h*0.425, "now_building", self.gamemaster, 0, 0, id))
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_workerTrd_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_workerTrd_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), image3=("RTS_build_bulding_ui", (w, int(w*502/1535)), 0, h-int(w*502/1535), 0,self.gamemaster,0,0,0,0), btn3=("RTS_cannon_red", (w*7/160, h*1/13.2), w*0.2, h*0.923, 'create_cannon', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn4=("RTS_healer_red", (w*7/210, h*1/14), w*0.14, h*0.9255, 'create_healer', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn6=("RTS_wall_red", (w*7/160, h*1/13.2), w*0.268, h*0.923, 'create_wall', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y), btn5=("RTS_fusion_btn", (w*6/67, h*1/18.2), w*0.45600,  h*0.94, 'FUSIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONTRD', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
    
    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

    def fusion(self,id):
        self.fusiontargetid = id
        self.gamemaster.move(self.id,self.gamemaster.sprite_list[id].pos_absolu_x,self.gamemaster.sprite_list[id].pos_absolu_y)


    def mine(self,id):
        # permé d'indiquer la pos du minerai ainsi que calculer le chemin a parcourir
        
        if self.depo_pos != (-1, -1):
            self.mine_targer = id
            self.first = True
            self.etape = 0
            self.to_mine = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) ,(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)))
            self.to_base = algo.search_path(np.array(self.gamemaster.fence.grille),(int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x / 10), int(self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y / 10)),(int(self.depo_pos[0]/ 10), int(self.depo_pos[1] / 10)) )
            del self.to_mine[0]
            del self.to_base[0]
            self.type= self.gamemaster.sprite_list[self.mine_targer].type

    def target_nexus(self,coord_x,coord_y):
        # permet de récuppéré ou les worker depose les minerai
        self.depo_pos = (coord_x,coord_y)
        try:
            self.mine()
        except:
            pass

    def mine_(self):
        # Verifie si le worker peut fusionner et fusione
        if self.fusiontargetid:
            if ((self.pos_absolu_x-self.gamemaster.sprite_list[self.fusiontargetid].pos_absolu_x)**2 + (self.pos_absolu_y-self.gamemaster.sprite_list[self.fusiontargetid].pos_absolu_y)**2)**(1/2) <=150:
                self.gamemaster.sprite_list[self.fusiontargetid].death()
                self.death()
                self.gamemaster.create_workerTrd(self.pos_absolu_x,self.pos_absolu_y,self.fusion_tier + self.gamemaster.sprite_list[self.fusiontargetid].fusion_tier)
                self.fusiontargetid = None
        if self.depo_pos != (-1,-1) and self.mine_targer != None :
            # Le minage est divisé en 3 ètape, la première étape consiste a revenir a la base la deuxième a aller au mine et la 3ème a miner
            if self.etape == 0:
                if ((self.pos_absolu_x - self.depo_pos[0])**2 + (self.pos_absolu_y - self.depo_pos[1])**2)**(1/2) <= 25 or self.first:
                    if self.type == 1 and self.allyteam:
                        self.gamemaster.iron += self.carry
                    if self.type == 2 and self.allyteam:
                        self.gamemaster.gold += self.carry
                    self.carry = 0
                    if self.first:
                        self.gamemaster.move( self.id,self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x, self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)
                    else:
                        self._path = copy.copy(self.to_mine)
                    self.etape = 1
                    self.first = False
            if self.etape == 1:

                if ((self.pos_absolu_x - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_x)**2 + (self.pos_absolu_y - self.gamemaster.sprite_list[self.mine_targer].pos_absolu_y)**2)**(1/2) <=30:
                    self.etape = 2
            if self.etape == 2:
                if self.type == 1:
                    self.carry += 2/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                elif self.type == 2:
                    self.carry += 0.2/self.gamemaster.clock.get_fps()
                    self.gamemaster.sprite_list[self.mine_targer].quantity -= 0.2/self.gamemaster.clock.get_fps()
                    if self.carry >= self.max_carry_gold:
                        self._path = copy.copy(self.to_base)
                        self.etape = 0
                if self.gamemaster.sprite_list[self.mine_targer].quantity <=0:
                    self.gamemaster.sprite_list[self.mine_targer].death()
                    self._path = copy.copy(self.to_base)
                    self.etape = 0

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class kamikaze(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 20, 0, 0.001, 6, 0, 10, 20, 1, "RTS_kamikaze_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id,_gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 20, 0, 0.001, 6, 0, 10, 20, 1, "RTS_kamikaze_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []
        self.gamemaster.actived_obj.append(self)

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_kamikaze_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_kamikaze_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), btn1=("BOOM", (w*3/16, h*1/14), w*0.17, h*0.923, 'boom', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_kamikaze_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_kamikaze_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0), btn1=("BOOM", (w*3/16, h*1/14), w*0.17, h*0.923, 'boom', self.gamemaster, self.pos_absolu_x, self.pos_absolu_y))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class debuff(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 40, 1, 1, 3, 10, 5, 20, 1, "RTS_debuff_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 40, 1, 1, 3, 10, 5, 20, 1, "RTS_debuff_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam
        self.debuff = False

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_debuff_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0, self.gamemaster,0,0,0,0), image2=("RTS_debuff_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_debuff_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_debuff_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
    
    def death(self):
        if self.target_enemy and self.debuff:
            n = self.gamemaster.sprite_list[self.target_enemy]
            n.ac *= 2
            n.dmg *= 2
            n.att_speed *= 2
            n.mvmt_speed *= 2
        self.gamemaster.supply-=self.supply_size
        self.delete_ui()
        self.gamemaster.suppr_sprite(self.id)
    
    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass
    
    def attack(self, id): # Remet à jour les statistiques de l'adversaires
        self.debuff = False
        if self.target_enemy and self.debuff:
            n = self.gamemaster.sprite_list[self.target_enemy]
            n.ac *= 2
            n.dmg *= 2
            n.att_speed *= 2
            n.mvmt_speed *= 2
        self.target_enemy = id

    def attack_(self): # Diminue les caractéristiques adverses par 2
        try:
            n = self.gamemaster.sprite_list[self.target_enemy]

            if self.target_enemy and ((n.pos_absolu_x-self.pos_absolu_x)**2+(n.pos_absolu_y-self.pos_absolu_y)**2)**(1/2) <= self.reach*10 :
                if self.debuff == False:
                    n.ac /= 2
                    n.dmg /= 2
                    n.att_speed /= 2
                    n.mvmt_speed //= 2
                    self.cooldown_attack = 1/self.att_speed
                    self.debuff = True

            if self.gamemaster.clock.get_fps():
                self.cooldown_attack -= 1/self.gamemaster.clock.get_fps()
                
        except KeyError:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class lowcost(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 10, 5, 2, 4, 3, 0, 20, 1, "RTS_lowcost_blue", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 10, 5, 2, 4, 3, 0, 20, 1, "RTS_lowcost_red", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_lowcost_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_lowcost_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_lowcost_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_lowcost_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class tank(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 100, 15, 1, 3, 5, 50, 20, 1, "RTS_tank_blue", None, (80, 80), x, y, 90, 90, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 100, 15, 1, 3, 5, 50, 20, 1, "RTS_tank_red", None, (80, 80), x, y, 90, 90, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_tank_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_tank_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_tank_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_tank_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class fast_troop(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 25, 2, 4, 9, 3, 0, 20, 1, "RTS_fast_troop_blue", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 25, 2, 4, 9, 3, 0, 20, 1, "RTS_fast_troop_red", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_fast_troop_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_fast_troop_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_fast_troop_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_fast_troop_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class fast_troop_archery(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 100, 10, 3, 5, 10, 0, 20, 1, "RTS_fast_troop_archery_blue", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 100, 10, 3, 5, 10, 0, 20, 1, "RTS_fast_troop_archery_red", None, (50, 50), x, y, 60, 60, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_fast_troop_archery_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_fast_troop_archery_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_fast_troop_archery_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_fast_troop_archery_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class archery(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 25, 7, 1, 3, 12, 0, 20, 1, "RTS_archery_blue", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 25, 7, 1, 3, 12, 0, 20, 1, "RTS_archery_red", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_archery_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_archery_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_archery_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_archery_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass

# MEME FONCTIONNEMENT QUE LA CLASSE Worker, VOIR SON COMMMENTAIRE SI BESOIN, LES FONCTIONS QUI DIFFERENT SONT COMMENTES
class siege_troop(Troops):
    def __init__(self, x, y, _gamemaster, id, idteam):
        if idteam==1:
            Troops.__init__(self, 75, 15, 0.2, 1, 150, 25, 20, 1, "RTS_siege_troop_blue", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] == 'BLUE')
        else:
            Troops.__init__(self, 75, 15, 0.2, 1, 150, 25, 20, 1, "RTS_siege_troop_red", None, (30, 30), x, y, 40, 40, _gamemaster, id, _gamemaster.ntw.team[0] != 'BLUE')

        self.gamemaster=_gamemaster
        self.depo_pos = (500,500)
        self.etape = 0
        self.first = False
        self.idteam = idteam

    def generate_ui(self):
        w, h = pygame.display.get_surface().get_size()
        self.gamemaster.actived_obj = []

        self.gamemaster.actived_obj.append(self)
        if self.idteam == 1:
            if self.action_ui:  
                pass                           
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_siege_troop_blue", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_siege_troop_blue", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))
        else:
            if self.action_ui:   
                pass  
            else:
                self.building_opt = UI.Object_UI(classe=self.gamemaster, image1=("RTS_siege_troop_red", (w*1/16, h*1/9), w*0.01, h*0.88, 0,self.gamemaster,0,0,0,0), image2=("RTS_siege_troop_red", (w*1/16, h*1/9),  w*0.925,  h*0.88, 0,self.gamemaster,0,0,0,0))

    def delete_ui(self):
        try:
            self.building_opt.delete_ui()
        except:
            pass
