Cython==3.0.8
numpy==1.26.4
pygame==2.5.2
setuptools==68.2.0
pathfinding==1.0.9

# installer c++ pour l’utilisation de cython via le lien :  https://tube-sciences-technologies.apps.education.fr/w/ieUo7p89rsNoM79qQhPdnY
